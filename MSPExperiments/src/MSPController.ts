import * as sp from './AsyncSerialPort';
import codes from './msp_commands.json';

const port = sp.connect('COM9', 115200);

let _data: any[] = [];

const main = async () => {
    /*
    // arm that shit
    const armObj: RCObj = {
        roll: 1500,
        pitch: 1500,
        yaw: 1500,
        throttle: 1000,
        aux1: 1000,
        aux2: 2000,
        aux3: 1000,
        aux4: 1000
    }

    setRawRC(armObj);

    setTimeout(() => {
        const disarmObj: RCObj = {
            roll: 1500,
            pitch: 1500,
            yaw: 1500,
            throttle: 1000,
            aux1: 1000,
            aux2: 1000,
            aux3: 1000,
            aux4: 1000
        }
        setRawRC(disarmObj);
    }, 2000);


*/
    setInterval(async () => {
        console.log(await getAttitude());
    }, 1000);    

}

const getAttitude = async () => {
    const response = await sp.send(packCMD(codes.MSP_ATTITUDE));
    if(Buffer.isBuffer(response)) {
        const unpacked = unpackResponse(response);
        console.log(unpacked);
        // convert attitude to readable stuff
        const parsed = {
            x: unpacked.data.readInt16LE(0) / 10,
            y: unpacked.data.readInt16LE(2) / 10,
            heading: unpacked.data.readInt16LE(4)
        }

        return parsed;
    }
}

interface RCObj {
    roll: number,
    pitch: number,
    yaw: number,
    throttle: number,
    aux1: number,
    aux2: number,
    aux3: number,
    aux4: number
}

const setRawRC = async (dataObj: RCObj) => {
    var data = new Buffer(16);

    data.writeUInt16LE(dataObj.roll, 0);
    data.writeUInt16LE(dataObj.pitch, 2);
    data.writeUInt16LE(dataObj.yaw, 4);
    data.writeUInt16LE(dataObj.throttle, 6);
    data.writeUInt16LE(dataObj.aux1, 8);
    data.writeUInt16LE(dataObj.aux2, 10);
    data.writeUInt16LE(dataObj.aux3, 12);
    data.writeUInt16LE(dataObj.aux4, 14);

    const response = await sp.send(packCMD(codes.MSP_SET_RAW_RC, data));
    console.log(response);

}

const packCMD = (code: number, data?: Buffer): Buffer => {

    const length = data === undefined || data === null ? 0 : data.length;
    const buffer = new Buffer(6 + length);
    buffer.write('$M<');
    buffer.writeUInt8(length, 3);
    buffer.writeUInt8(code, 4);
    let crc = 0x00 ^ code ^ length;

    if(data) {
        for(let i = 0; i < length; i++) {
            crc ^= data.readUInt8(i);
            buffer.writeUInt8(data.readUInt8(i), i + 5);
        }
    }


    buffer.writeUInt8(crc, buffer.length - 1);

    return buffer;
}

const unpackResponse = (data?: any) => {
    var i, length, code, crc, offset, valid, response;

    if (data) {
        for (i = 0; i < data.length; i = i + 1) {
            _data[_data.length] = data.readUInt8(i);
        }
    }

    valid = false;
    offset = 0;
    while (offset < _data.length) {
        if (_data[offset] !== 36) {
            offset = offset + 1;
        } else if (_data[offset + 1] !== 77) {
            offset = offset + 2;
        } else if (_data[offset + 2] !== 62) {
            offset = offset + 3;
        } else if (_data[offset + 3] <= _data.length - 6 - offset) {
            length = _data[offset + 3];
            code = _data[offset + 4];
            crc = 0x00 ^ code ^ length;

            for (i = 0; i < length; i = i + 1) {
                crc ^= _data[offset + 5 + i];
            }

            if (crc !== _data[offset + 5 + length]) {
                offset = offset + 5 + length;
            } else {
                data = new Buffer(length);
                for (i = 0; i < length; i = i + 1) {
                    data.writeUInt8(_data[offset + 5 + i], i);
                }

                valid = true;
                offset = offset + 5 + length + 1;
                break;
            }
        } else {
            break;
        }
    }

    _data = _data.slice(offset);

    response = {
        valid: valid,
        code,
        length,
        data
    };

    if (valid) {
        response.code = code;
        response.length = length;
        response.data = data;
    }

    return response;
};

main();

