import SerialPort from 'serialport';

let port: SerialPort;

export const connect = (path: string, baud: number) => {
    port = new SerialPort(path, {
        baudRate: baud
    });
    return port;
}

export const send = (data: Buffer) => {    
    return new Promise((resolve, reject) => {        
        port.write(data, () => {
            console.log('Sent', data);
            port.flush();
            port.on('data', (rdata?:Buffer) => {
                if(rdata) {           
                    port.flush();                             
                    resolve(rdata);
                } else {
                    port.flush();
                    resolve(new Buffer(0)); // return empty buffer if no data
                }
            });
            port.on('error', (err) => {
                reject(err);
            });
        });
    })
}

