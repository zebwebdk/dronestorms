# Untitled - By: sthaz - on mar 6 2019

import sensor, image, time
from pyb import delay
from pymultiwii import MultiWii



thresholds = [(30, 100, 15, 127, 15, 127), (76, 100, -107, -15, 3, 127), (67, 100, -128, -10, -128, 8)]

# til afstandsberegning
lens_mm = 2.8 #std
avg_obj_height_mm = 115
image_height_pixels = 240.0
sensor_h_mm = 2.952
offset_mm = 100.0

def rect_size_to_distance(r):
    return ((lens_mm * avg_obj_height_mm * image_height_pixels) / (r[3] * sensor_h_mm)) - offset_mm

sensor.reset()
sensor.set_pixformat(sensor.RGB565)
sensor.set_framesize(sensor.QVGA)
sensor.skip_frames(time = 2000)

clock = time.clock()

msp = MultiWii()

while(True):
    img = sensor.snapshot()
    largestBlob = None
    largestBlobArea = 0
    for blob in img.find_blobs(thresholds, pixels_threshold=500, area_threshold=500):
        if blob.area() > largestBlobArea :
            largestBlob = blob

    #tegn kun den største blob
    if largestBlob != None:
        img.draw_rectangle(largestBlob.rect())
        img.draw_cross(largestBlob.cx(), largestBlob.cy())
        img.draw_string(largestBlob.cx(), largestBlob.cy(), "Distance %d mm" % rect_size_to_distance(largestBlob.rect()))

        #print("Blob dims: Center: " + str(largestBlob.cx()) + ", " +str(largestBlob.cy()) + ", Area: " + str(largestBlob.area()))

    msp.sendCMD(16, msp.SET_RAW_RC, [1500, 1500, 1000, 1500, 1000, 0, 0, 1000])

    # hent attitude
    currentAlt = msp.getAlt()
    print("Quad Alt: " + str(currentAlt))
    # print(clock.fps())
