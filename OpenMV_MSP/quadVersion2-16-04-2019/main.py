# Quad Controller with TCP Protocl support
#
import sensor, image, time, network, ubinascii, json, usocket, uselect, uerrno, gc
from pymultiwii import MultiWii

from pyb import LED, millis

red_led   = LED(1)

#SSID='korrosion' # Network SSID
#KEY='CorrectHorseBatteryStaple'  # Network key

SSID='OnePlus5T' # Network SSID
KEY='hotspot42'  # Network key

# Init wlan module and connect to network
print("Trying to connect... (may take a while)...")

wlan = network.WINC()
wlan.connect(SSID, key=KEY, security=wlan.WPA_PSK)

# We should have a valid IP now via DHCP
print(wlan.ifconfig())

# setup TCP connection
#addr = usocket.getaddrinfo("192.168.1.176", 1337)[0][4]
addr = usocket.getaddrinfo("192.168.43.4", 1337)[0][4]
server_socket = usocket.socket(usocket.AF_INET, usocket.SOCK_STREAM)
server_socket.connect(addr)
server_socket.setblocking(0)
server_socket.settimeout(0)

a = 1500
e = 1500
r = 1500
t = 1000
aux1 = 1000

# navigation commands
def getQuadAttitude():
    msp.getData(MultiWii.ATTITUDE)
    print('Attitude', msp.attitude)

def getQuadAltitude():
    msp.getData(MultiWii.ALTITUDE)
    print('Altitude', msp.altitude)

def armQuad():
    global aux1
    aux1 = 2000
    print(str([a, e, t, r, aux1]))
    end = millis() + 2000
    while millis() < end:
        sendRC([a, e, t, r, aux1])
    print('done arm')
    sendQuadOutput("Quad Armed")

def disarmQuad():
    global aux1
    aux1 = 1000
    print(str([a, e, t, r, aux1]))
    end = millis() + 2000
    while millis() < end:
        sendRC([a, e, t, r, aux1])
    print('done disarm')
    sendQuadOutput("Quad Disarmed")

def raiseQuad(seconds):
    global t
    print("Setting quad raise for", str(seconds), "seconds")
    sendQuadOutput("Setting quad raise for " + str(seconds) + " seconds")
    t = 1800
    end = millis() + (seconds * 1000)
    while millis() < end:
        sendRC([a, e, t, r, aux1])

def hoverQuad(seconds):
    global t
    print("Hovering for", str(seconds), "seconds")
    sendQuadOutput("Hovering for " + str(seconds) + " seconds")
    t = 1500
    end = millis() + (seconds * 1000)
    while millis() < end:
        sendRC([a, e, t, r, aux1])

def lowerQuad(seconds):
    global t
    print("Setting quad lower for", str(seconds), "seconds")
    sendQuadOutput("Setting quad lower for " + str(seconds) + " seconds")
    t = 1200
    end = millis() + (seconds * 1000)
    while millis() < end:
        sendRC([a, e, t, r, aux1])

def quadRight(seconds):
    global a
    print("Setting quad right for", str(seconds), "seconds")
    sendQuadOutput("Setting quad right for " + str(seconds) + " seconds")
    a = 1600
    end = millis() + (seconds * 1000)
    while millis() < end:
        sendRC([a, e, t, r, aux1])

def quadLeft(seconds):
    global a
    print("Setting quad left for", str(seconds), "seconds")
    sendQuadOutput("Setting quad left for " + str(seconds) + "seconds")
    a = 1400
    end = millis() + (seconds * 1000)
    while millis() < end:
        sendRC([a, e, t, r, aux1])

def quadFwd(seconds):
    global e
    print("Setting quad fwd for", str(seconds), "seconds")
    sendQuadOutput("Setting quad fwd for " + str(seconds) + " seconds")
    e = 1600
    end = millis() + (seconds * 1000)
    while millis() < end:
        sendRC([a, e, t, r, aux1])

def quadBack(seconds):
    global e
    print("Setting quad back for", str(seconds), "seconds")
    sendQuadOutput("Setting quad back for " + str(seconds) + " seconds")
    e = 1400
    end = millis() + (seconds * 1000)
    while millis() < end:
        sendRC([a, e, t, r, aux1])

def sendRC(data):
    msp.sendCMD(10, msp.SET_RAW_RC, data)

def setHead(head):
    print("Setting heading at :", str(head))
    sendQuadOutput("Setting heading to " + str(head))
    msp.setHeading(head)

def sendQuadOutput(output):
    global server_socket
    #msg = '{"output": "' + output + '"}'
    packagedOutput = {"output": output}
    print(str(packagedOutput))
    server_socket.send(json.dumps(packagedOutput))


# message handler
def handleMSG(msg):
    if msg != None and msg != '':
        msg = msg.decode('utf-8')
        print("Raw Msg", str(msg))
        if msg == '':
            return
        try:
            parsed = json.loads(msg)
            print('Decoded Command', parsed['command'])

            if parsed['command'] == 'CODE':
                global a, e, r, t, aux1
                a = 1500
                e = 1500
                t = 1000
                r = 1500
                aux1 = 1000
                # make safe signal by sending base rc for 1 second
                end = millis() + 1000
                while millis() < end:
                    sendRC([a, e, t, r, aux1])

                print('Executing Code')
                red_led.on()
                exec(parsed['code'])
                red_led.off()
                gc.collect()
        except OSError as error:
            print("MSG ERROR", error)

# load msp
msp = MultiWii()

## setup sensor
sensor.reset()                      # Reset and initialize the sensor.
sensor.set_pixformat(sensor.RGB565) # Set pixel format to RGB565 (or GRAYSCALE)
sensor.set_framesize(sensor.B64X32) # Set frame size to 64x32... (or 64x64)...
sensor.skip_frames(time = 2000)     # Wait for settings take effect.
clock = time.clock()                # Create a clock object to track the FPS.

# Take from the main frame buffer's RAM to allocate a second frame buffer.
# There's a lot more RAM in the frame buffer than in the MicroPython heap.
# However, after doing this you have a lot less RAM for some algorithms...
# So, be aware that it's a lot easier to get out of RAM issues now.
extra_fb = sensor.alloc_extra_fb(sensor.width(), sensor.height(), sensor.RGB565)
extra_fb.replace(sensor.snapshot())


while True:
    handleMSG(server_socket.recv(512))

    img = sensor.snapshot() # Take a picture and return the image.

    displacement = extra_fb.find_displacement(img)
    extra_fb.replace(img)

    # Offset results are noisy without filtering so we drop some accuracy.
    sub_pixel_x = int(displacement.x_translation() * 5) / 5.0
    sub_pixel_y = int(displacement.y_translation() * 5) / 5.0


    print("{0:+f}x {1:+f}y {2} {3} FPS".format(sub_pixel_x, sub_pixel_y,
          displacement.response(),
