# Opflow Test

import sensor, image, time
from pymultiwii import MultiWii



sensor.reset()                      # Reset and initialize the sensor.
sensor.set_pixformat(sensor.RGB565) # Set pixel format to RGB565 (or GRAYSCALE)
sensor.set_framesize(sensor.B64X32) # Set frame size to 64x32... (or 64x64)...
sensor.skip_frames(time = 2000)     # Wait for settings take effect.
clock = time.clock()                # Create a clock object to track the FPS.

# Take from the main frame buffer's RAM to allocate a second frame buffer.
# There's a lot more RAM in the frame buffer than in the MicroPython heap.
# However, after doing this you have a lot less RAM for some algorithms...
# So, be aware that it's a lot easier to get out of RAM issues now.
extra_fb = sensor.alloc_extra_fb(sensor.width(), sensor.height(), sensor.RGB565)
extra_fb.replace(sensor.snapshot())

# load msp
msp = MultiWii()

while(True):
    clock.tick() # Track elapsed milliseconds between snapshots().
    img = sensor.snapshot() # Take a picture and return the image.

    displacement = extra_fb.find_displacement(img)
    extra_fb.replace(img)

    # Offset results are noisy without filtering so we drop some accuracy.

#    sub_pixel_x = int(-displacement.x_translation() * 35)
#    sub_pixel_y = int(displacement.y_translation() * 53)

    sub_pixel_x = int(int(displacement.x_translation() * 5) / 5.0)
    sub_pixel_y = int(int(displacement.y_translation() * 5) / 5.0)
    quality = int(displacement.response() * 255.0)

    print("{0}x {1}y {2} {3} FPS".format(sub_pixel_x, sub_pixel_y, quality, clock.fps()))
    msp.sendOPFLOW(sub_pixel_x, sub_pixel_y, quality)
    print()
    print()


