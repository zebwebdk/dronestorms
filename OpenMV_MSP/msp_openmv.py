"""
port of specific functions from the PyMultiWii library MSP implementation
for the OpenMV Micropython platform

2019 ZEB
"""

import struct
from pyb import UART

class MultiWii:

    SET_RAW_RC = 200

    def __init__(self):
        self.uart = UART(3, 115200, timeout_char=1000)

    def sendCMD(self, data_length, code, data):
        checksum = int(0)
        total_data = ['$', 'M', '<', data_length, code] + data
        for i in struct.pack('<2B%dH' % len(data), *total_data[3:len(total_data)]):
                checksum = checksum ^ ord(chr(i))
                total_data.append(checksum)
                structPack = struct.pack('<3c2B%dHB' % len(data), *total_data)
                print ("The string we are writing, : " + structPack)                    
                try:
                    b = None                    
                    b = self.uart.write(struct.pack('<3c2B%dHB' % len(data), *total_data))
                except Exception as error:
                    print ("\n\nError in sendCMD.")
                    print ("("+str(error)+")\n\n")
                pass
