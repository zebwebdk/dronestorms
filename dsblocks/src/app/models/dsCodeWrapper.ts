
const header = `
#!/usr/bin/env python2

""" DroneStorms CustomScript class
    (c) 2019 Team Awesome Specialists
    Author: ZEB
    Description:
    Code container to present code to the main script that needs to run on the DroneStorms QuadCopter
    """

class CustomScript():    

    def __init__(self):
        self.running = True
    
    def getRunning(self):
        return self.running

    def run(self):
        # Code is inserted here ... 
`;

const footer = `
        # Must end with running = False to exit script
        self.running = False
`

export const DSCodeWrapper = (code: string) => {
    return header + code + footer;
}