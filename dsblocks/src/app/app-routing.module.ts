import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BlocklyMainComponent } from './blockly-main/blockly-main.component';

const routes: Routes = [
  {
    path: '', component: BlocklyMainComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
