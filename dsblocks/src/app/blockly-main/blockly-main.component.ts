import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';

import { DSCodeWrapper } from '../models/dsCodeWrapper';
import { writeFileSync } from 'fs';

declare let Blockly: any;

@Component({
  selector: 'dsb-blockly-main',
  templateUrl: './blockly-main.component.html',
  styleUrls: ['./blockly-main.component.scss']
})
export class BlocklyMainComponent implements OnInit, AfterViewInit {

  @ViewChild('blocklyDiv') blocklyDiv: ElementRef;
  @ViewChild('codeDisplay') codeDisplay: ElementRef;

  public workspace;

  toolboxXML = `
  <xml id="toolbox" style="display: none">
    <block type="controls_if"></block>
    <block type="controls_repeat_ext"></block>
    <block type="logic_compare"></block>
    <block type="math_number"></block>
    <block type="math_arithmetic"></block>
    <block type="text"></block>
    <block type="text_print"></block>
  </xml>
  `;

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    console.log('View was init - bootstrapping blockly');
    console.log('BlocklyDiv', this.blocklyDiv);

    this.workspace = Blockly.inject('blocklyDiv', {
      toolbox: this.toolboxXML
    });

    this.workspace.addChangeListener(this.generateCode);

  }

  generateCode() {
    console.log('chaange!');
    if(this.workspace) {
      const code = Blockly.Python.workspaceToCode(this.workspace);
      this.codeDisplay.nativeElement.insertAdjacentHTML('beforeend', code);
    }    
  }

  exportCodeToDevice() {
    console.log('Exporting code...')
    if(this.workspace) {
      const code = Blockly.Python.workspaceToCode(this.workspace);
      // wrap code into our function
      const exportedCode = DSCodeWrapper(code);
      writeFileSync(__dirname + '/customScript.py', exportedCode);
      console.log('File Written');
    }
  }


}
