import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlocklyMainComponent } from './blockly-main.component';

describe('BlocklyMainComponent', () => {
  let component: BlocklyMainComponent;
  let fixture: ComponentFixture<BlocklyMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlocklyMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlocklyMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
