# Quad Controller with TCP Protocol support
#
import sensor, json, image, time, network, ubinascii, usocket, uheapq
from pyb import LED, millis
from math import sqrt

from lib.pymultiwii import MultiWii
from lib.StateMonitor import StateMonitor
from lib.Navigator import Navigator
from lib.Communicator import Communicator
from lib.mqtt import MQTTClient

# Setup Networking ## this is needed before module init
### DEV NETWORK SETTINGS
SSID='korrosion'
KEY='CorrectHorseBatteryStaple'
### IN THE WILD SETTINGS
#SSID='OnePlus5T'
#KEY='hotspot42'
# Init wlan module and connect to network
print("Trying to connect... (may take a while)...")
wlan = network.WINC()
wlan.connect(SSID, key=KEY, security=wlan.WPA_PSK)
# We should have a valid IP now via DHCP
print(wlan.ifconfig())

# Init imported modules and objects
red_led   = LED(1)
stateMonitor = StateMonitor()
msp = MultiWii()

# setup TCP connection
addr = usocket.getaddrinfo("192.168.1.176", 1338)[0][4]
#addr = usocket.getaddrinfo("192.168.43.4", 1337)[0][4]
server_socket = usocket.socket()
server_socket.connect(addr)
#server_socket.bind(addr)
#server_socket.send('EY YOOO')
#communicator = Communicator(server_socket)


client = MQTTClient("dronestorms", "192.168.1.176", port=1337, keepalive=15000)
client.connect()
communicator = Communicator(server_socket)

navigator = Navigator(msp, communicator, stateMonitor)

commandQueue = []
#uheapq.heapify(commandQueue)

# message handler
def handleMSG(topic, msg):
    global commandQueue
    topic = topic.decode('utf-8')
    msg = msg.decode('utf-8')
    print("Topic", topic, "MSG:", msg)
    if topic == 'CODE':
        navigator.sendSafeRC()
        commandQueue = list(filter(None, msg.split('\n')))
        commandQueue.reverse()
        #uheapq.heapify(commandQueue)
        #navigator.sendSafeRC()
        #print("Executing Code")
        #red_led.on()
        #exec(code)
        #red_led.off()

def OpFlowRoutine():
    global img
    #img = sensor.snapshot() # Take a picture and return the image.

    displacement = extra_fb.find_displacement(img)
    extra_fb.replace(img)


    # Offset results are noisy without filtering so we drop some accuracy.
    sub_pixel_x = int(displacement.x_translation() * 5) / 5.0
    sub_pixel_y = int(displacement.y_translation() * 5) / 5.0

    msp.sendOPFLOW(int(sub_pixel_x), int(sub_pixel_y), int(displacement.response() * 255.0))

    #print("{0:+f}x {1:+f}y {2} {3} FPS".format(sub_pixel_x, sub_pixel_y,
    #      displacement.response(),
    #      clock.fps()))

def TagRoutine():
    global img
    for tag in img.find_apriltags(): # defaults to TAG36H11 without "families".
        img.draw_rectangle(tag.rect(), color = (255, 0, 0))
        img.draw_cross(tag.cx(), tag.cy(), color = (0, 255, 0))
        # we want to center the camera on the tag - what should we do?
        # center point of image is 32x32 -
        #print("Tag Coords:", tag.cx(), tag.cy())
        #x less than 32 move fwd more than 32 move bck
        #y less than 32 move right more than 32 move left
        #calculate vector towards middle
        # normalize so my coords are 0, 0
        # center is 32, 32 - which means center is now
        targetX = 32 - tag.cx()
        targetY = 32 - tag.cy()
        # calculate direction and distance
        dist = sqrt( (targetX)**2 + (targetY)**2 )
        print("Target Coords", targetX, targetY, "distance", dist)
        # draw a line towards center
        img.draw_line(32, 32, tag.cx(), tag.cy(), color = (0,0,255))

        #x less than 32 move fwd more than 32 move bck
        #y less than 32 move right more than 32 move left
        if targetX > 5:
            print("Move FWD")
        elif targetX < -5:
            print("Move Back")
        else:
            print("X On target")

        if targetY < -5:
            print("Move right")
        elif targetY > 5:
            print("Move left")
        else:
            print("Y On Target!")



def CheckAltitude():
    #msp.getRangefinderData()
    msp.getData(msp.ALTITUDE)
    print("ALT:", msp.altitude)

def SendVideoFeed():
    global img
    print("image stuff!")
    base63_data = ubinascii.b2a_base64(img.compress(quality=40))
    print(base63_data)

def GetBoxNames():
    names = msp.getData(msp.BOXNAMES)
    alts = msp.getData(msp.SONARALT)

## setup sensor
sensor.reset()                      # Reset and initialize the sensor.
sensor.set_pixformat(sensor.RGB565) # Set pixel format to RGB565 (or GRAYSCALE)
sensor.set_framesize(sensor.B64X64) # Set frame size to 64x32... (or 64x64)...
sensor.skip_frames(time = 2000)     # Wait for settings take effect.
clock = time.clock()                # Create a clock object to track the FPS.
img = sensor.snapshot()

# TODO: Fix out of memory errors!
# Take from the main frame buffer's RAM to allocate a second frame buffer.
# There's a lot more RAM in the frame buffer than in the aicroPython heap.
# However, after doing this you have a lot less RAM for some algorithms...
# So, be aware that it's a lot easier to get out of RAM issues now.
#extra_fb = sensor.snapshot() #sensor.alloc_extra_fb(sensor.width(), sensor.height(), sensor.RGB565)
#extra_fb.replace(sensor.snapshot())

client.set_callback(handleMSG)
# set routines to be run during the command loop
#navigator.setLoopRoutines(OpFlowRoutine, TagRoutine, CheckAltitude)
#navigator.setLoopRoutines(OpFlowRoutine, CheckAltitude)
#navigator.setLoopRoutines(OpFlowRoutine)
navigator.setLoopRoutines(TagRoutine)


while True:
    img = sensor.snapshot()
    client.ping()
    client.check_msg()

    if len(commandQueue) > 0:
        # we have commands - execute a command
        print("There are", len(commandQueue), "commands in line")
        cmd = commandQueue.pop()
        print("executing", cmd)
        exec(cmd)
    else:
        navigator.doLoopRoutines()

