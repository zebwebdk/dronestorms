
class StateMonitor():

    maxAngle = 15

    def CheckIfValid(self, attitude):        
        if self.CheckAttitude(attitude) == True:
            if self.CheckConnection() == True:
                return True
            else:
                return False
        else:
            return False

    def CheckAttitude(self, attitude):        
        angy = float(attitude['angy'])
        angx = float(attitude['angx'])
        #print("angles", angy, angx, "maxAngle", self.maxAngle, -self.maxAngle)

        if angy < self.maxAngle and angy > -self.maxAngle and angx < self.maxAngle and angx > -self.maxAngle:
            return True
        else:
            print("Angle is out of bounds!")
            return False

    # TODO: fix connection check - this could probably be done using the ping function from mqtt
    def CheckConnection(self):
        # Send ConnCheckPackage to PC
        # If response is valid
        # return true
        # else
        # return false
        # print("Checking Connection")
        return True
