"""
    DroneStorms navigator class
    All functions callable from DSBlocks will be stated here
"""
from pyb import millis

class Navigator():
    isAllowedToFly = False
    a = 1500
    e = 1500
    r = 1500
    t = 1000
    aux1 = 1000

    def __init__(self, msp, communicator, stateMonitor):
        self.msp = msp
        self.communicator = communicator
        self.stateMonitor = stateMonitor

    def setLoopRoutines(self, *routines):
        self.routines = routines

    def getQuadAttitude(self):
        self.msp.getData(self.msp.ATTITUDE)
        return self.msp.attitude

    def getQuadAltitude(self):
        self.msp.getData(self.msp.ALTITUDE)
        return self.msp.altitude

    def armQuad(self):    
        self.aux1 = 2000        
        end = millis() + 2000
        while millis() < end:
            self.sendRC([self.a, self.e, self.t, self.r, self.aux1])
        print('done arm')
        self.communicator.sendQuadOutput("Quad Armed")

    def disarmQuad(self):        
        self.aux1 = 1000        
        end = millis() + 2000
        while millis() < end:
            self.sendRC([self.a, self.e, self.t, self.r, self.aux1])
        print('done disarm')
        self.communicator.sendQuadOutput("Quad Disarmed")        

    def raiseQuad(self, seconds):        
        print("Setting quad raise for", str(seconds), "seconds")
        self.communicator.sendQuadOutput("Setting quad raise for " + str(seconds) + " seconds")
        self.t = 1800
        end = millis() + (seconds * 1000)
        while millis() < end:
            self.sendRC([self.a, self.e, self.t, self.r, self.aux1])

    def hoverQuad(self, seconds):        
        print("Hovering for", str(seconds), "seconds")
        self.communicator.sendQuadOutput("Hovering for " + str(seconds) + " seconds")
        self.t = 1500
        end = millis() + (seconds * 1000)
        while millis() < end:
            self.sendRC([self.a, self.e, self.t, self.r, self.aux1])

    def lowerQuad(self, seconds):        
        print("Setting quad lower for", str(seconds), "seconds")
        self.communicator.sendQuadOutput("Setting quad lower for " + str(seconds) + " seconds")
        self.t = 1200
        end = millis() + (seconds * 1000)
        while millis() < end:
            self.sendRC([self.a, self.e, self.t, self.r, self.aux1])

    def quadRight(self, seconds):        
        print("Setting quad right for", str(seconds), "seconds")
        self.communicator.sendQuadOutput("Setting quad right for " + str(seconds) + " seconds")
        self.a = 1600
        end = millis() + (seconds * 1000)
        while millis() < end:
            self.sendRC([self.a, self.e, self.t, self.r, self.aux1])


    def quadLeft(self, seconds):        
        print("Setting quad left for", str(seconds), "seconds")
        self.communicator.sendQuadOutput("Setting quad left for " + str(seconds) + "seconds")
        self.a = 1400
        end = millis() + (seconds * 1000)
        while millis() < end:
            self.sendRC([self.a, self.e, self.t, self.r, self.aux1])

    def quadFwd(self, seconds):        
        print("Setting quad fwd for", str(seconds), "seconds")
        self.communicator.sendQuadOutput("Setting quad fwd for " + str(seconds) + " seconds")
        self.e = 1600
        end = millis() + (seconds * 1000)
        while millis() < end:
            self.sendRC([self.a, self.e, self.t, self.r, self.aux1])

    def quadBack(self, seconds):        
        print("Setting quad back for", str(seconds), "seconds")
        self.communicator.sendQuadOutput("Setting quad back for " + str(seconds) + " seconds")
        self.e = 1400
        end = millis() + (seconds * 1000)
        while millis() < end:
            self.sendRC([self.a, self.e, self.t, self.r, self.aux1])

    ## Safety stuff and RC Send commands

    def killQuad(self):
        data = [1500, 1500, 1000, 1500, 1000]
        self.msp.sendCMD(10, self.msp.SET_RAW_RC, data)
        self.communicator.sendQuadOutput("Quad Killed!")        

    def sendSafeRC(self):
        self.isAllowedToFly = True
        end = millis() + 1000
        while millis() < end:
            self.sendRC([1500, 1500, 1000, 1500, 1000])

    def doLoopRoutines(self):
        if len(self.routines) > 0:
            for r in self.routines:
                r()

    def sendRC(self, data):
        self.doLoopRoutines()
        
        if self.stateMonitor.CheckIfValid(self.getQuadAttitude()) and self.isAllowedToFly:
            self.msp.sendCMD(10, self.msp.SET_RAW_RC, data)
        else:
            self.isAllowedToFly = False
            self.communicator.sendQuadOutput("Quad State is Invalid!")
            self.killQuad()
        
        #if self.stateMonitor.CheckIfValid(self.getQuadAttitude()) == True and self.isAllowedToFly == True:
        #    self.communicator.sendStickPos(self.a, self.e, self.t, self.r)
        #    self.msp.sendCMD(10, self.msp.SET_RAW_RC, data)
        #else:
        #    self.isAllowedToFly = False
        #    self.communicator.sendQuadOutput("Quad State is Invalid!")
        #    self.killQuad()