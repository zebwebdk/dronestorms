import json
class Communicator():

    def __init__(self, socket):
        self.socket = socket

    def sendQuadOutput(self, output):                
        packagedOutput = {"output": output}        
        self.socket.send(json.dumps(packagedOutput))

    def sendStickPos(self, a, e, t, r): 
        stickOutputs = {"sticks": True, "a": a, "e": e, "t": t, "r": r}
        self.socket.send(json.dumps(stickOutputs))