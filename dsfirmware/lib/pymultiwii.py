#!/usr/bin/env python

"""multiwii.py: Handles Multiwii Serial Protocol."""

__author__ = "Aldo Vargas"
__copyright__ = "Copyright 2017 Altax.net"

__license__ = "GPL"
__version__ = "1.6"
__maintainer__ = "Aldo Vargas"
__email__ = "alduxvm@gmail.com"
__status__ = "Development"


import time, struct
from pyb import UART
from pyb import millis


class MultiWii:

    """Multiwii Serial Protocol message ID"""
    """ notice: just attitude, rc channels and raw imu, set raw rc are implemented at the moment """
    #INAV CLI SPECIFICS?
    SONARALT = 58
    # REGULAR V1 CODES
    IDENT = 100
    STATUS = 101
    RAW_IMU = 102
    SERVO = 103
    MOTOR = 104
    RC = 105
    RAW_GPS = 106
    COMP_GPS = 107
    ATTITUDE = 108
    ALTITUDE = 109
    ANALOG = 110
    RC_TUNING = 111
    PID = 112
    BOX = 113
    MISC = 114
    MOTOR_PINS = 115
    BOXNAMES = 116
    PIDNAMES = 117
    WP = 118
    BOXIDS = 119
    RC_RAW_IMU = 121
    SET_RAW_RC = 200
    SET_RAW_GPS = 201
    SET_PID = 202
    SET_BOX = 203
    SET_RC_TUNING = 204
    ACC_CALIBRATION = 205
    MAG_CALIBRATION = 206
    SET_MISC = 207
    RESET_CONF = 208
    SET_WP = 209
    SWITCH_RC_SERIAL = 210
    IS_SERIAL = 211
    DEBUG = 254

    # MSP v2 sensor codes
    RANGEFINDER = 7937
    OPFLOW = 7938

    """Class initialization"""
    def __init__(self):

        """Global variables of data"""
        self.PIDcoef = {'rp':0,'ri':0,'rd':0,'pp':0,'pi':0,'pd':0,'yp':0,'yi':0,'yd':0}
        self.rcChannels = {'roll':0,'pitch':0,'yaw':0,'throttle':0,'elapsed':0,'timestamp':0}
        self.rawIMU = {'ax':0,'ay':0,'az':0,'gx':0,'gy':0,'gz':0,'mx':0,'my':0,'mz':0,'elapsed':0,'timestamp':0}
        self.motor = {'m1':0,'m2':0,'m3':0,'m4':0,'elapsed':0,'timestamp':0}
        self.attitude = {'angx':0,'angy':0,'heading':0,'elapsed':0,'timestamp':0}
        self.altitude = {'estalt':0,'vario':0,'elapsed':0,'timestamp':0}
        self.message = {'angx':0,'angy':0,'heading':0,'roll':0,'pitch':0,'yaw':0,'throttle':0,'elapsed':0,'timestamp':0}
        self.analog = {'vbat':0,'intPowerMeterSum':0,'rssi':0,'amperage':0,'elapsed':0,'timestamp':0}
        self.temp = ();
        self.temp2 = ();
        self.elapsed = 0
        self.PRINT = 1

        '''self.ser = serial.Serial()
        self.ser.port = serPort
        self.ser.baudrate = 115200
        self.ser.bytesize = serial.EIGHTBITS
        self.ser.parity = serial.PARITY_NONE
        self.ser.stopbits = serial.STOPBITS_ONE
        self.ser.timeout = 0
        self.ser.xonxoff = False
        self.ser.rtscts = False
        self.ser.dsrdtr = False
        self.ser.writeTimeout = 2'''
        self.ser = UART(3, 115200, timeout_char=1000)
        """Time to wait until the board becomes operational"""
        wakeup = 2
        try:
            # self.ser.open()
            self.ser.init(115200, bits=8, parity=None, stop=1, timeout_char=1000)
            if self.PRINT:
                print ("Waking up board on UART")
            for i in range(1,wakeup):
                if self.PRINT:
                    print (wakeup-i)
                    time.sleep(1000)
                else:
                    time.sleep(1000)
        except Exception as error:
            print ("\n\nError opening UART.\n"+str(error)+"\n\n")

    """Function for sending a command to the board"""
    def sendCMD(self, data_length, code, data):
        checksum = int(0)
        total_data = [ord('$'), ord('M'), ord('<'), data_length, code] + data
        for i in struct.pack('<2B%dH' % len(data), *total_data[3:len(total_data)]):
            checksum = checksum ^ ord(chr(i))
        total_data.append(checksum)
        try:
            b = None
            #print ("The string we are writing, : " + str(struct.pack('<3B2B%dHB' % len(data), *total_data)))
            b = self.ser.write(struct.pack('<3B2B%dHB' % len(data), *total_data))
        except Exception as error:
            print ("\n\nError in sendCMD.")
            print ("("+str(error)+")\n\n")
            pass

    def sendCMDV2(self, payload_length, code, payload):
        checksum = int(0)
        total_data = [ord('$'), ord('X'), ord('<'), 0, code, payload_length] + payload        
        sendStruct = ''
        if code == MultiWii.OPFLOW: # OpFlow
            #for i in struct.pack('<B2H%dlh' % len(payload), *total_data[3:len(total_data)]):
            #FIXED PAYLOAD LENGTH LIGE NU... SORRY.
            for i in struct.pack('<B2Hh2l', *total_data[3:len(total_data)]):
                checksum = self.crc8_dvb_s2(checksum, i)

            total_data.append(checksum)            
            sendStruct = '<4B2Hh2lB'
        elif code == MultiWii.RANGEFINDER: # Rangefinder Code

            for i in struct.pack('<B2H', *total_data[3:len(total_data)]):
                checksum = self.crc8_dvb_s2(checksum, i)
                
            total_data.append(checksum)            
            sendStruct = '<4B2HB'

        try:
                b = None
                #print ("The string we are writing, : " + str(struct.pack('<4B2H%dlhB' % len(payload)-1, *total_data)))
                #b = self.ser.write(struct.pack('<4B2H%dlhB' % len(payload)-1, *total_data))


                        #FIXED PAYLOAD LENGTH LIGE NU... SORRY.

                #print ("The string we are writing, : " + str(struct.pack('<4B2Hh2lB', *total_data)))
                b = self.ser.write(struct.pack(sendStruct, *total_data))
        except Exception as error:
            print ("\n\nError in sendCMDV2.")
            print ("("+str(error)+")\n\n")
            pass


    def crc8_dvb_s2(self, crc, a):
        crc ^= ord(chr(a))
        for _ in range(8):
            if crc & 0x80:
                crc = ((crc << 1) ^ 0xD5) % 256
            else:
                crc = (crc << 1) % 256
        return crc

    """Function for sending OPFLOW MSP packages to board"""
    def sendOPFLOW(self, x, y, q):
    # number of bytes in payload: 4 +4 +2 = 10 
        self.sendCMDV2(10, MultiWii.OPFLOW, [q, x, y])

    """Function for sending a command to the board and receive attitude"""
    """
    Modification required on Multiwii firmware to Protocol.cpp in evaluateCommand:

    case MSP_SET_RAW_RC:
      s_struct_w((uint8_t*)&rcSerial,16);
      rcSerialCount = 50; // 1s transition
      s_struct((uint8_t*)&att,6);
      break;

    """
    def sendCMDreceiveATT(self, data_length, code, data):
        checksum = 0
        total_data = ['$'.encode('utf-8'), 'M'.encode('utf-8'), '<'.encode('utf-8'), data_length, code] + data
        for i in struct.pack('<2B%dH' % len(data), *total_data[3:len(total_data)]):
            checksum = checksum ^ ord(chr(i))
        total_data.append(checksum)
        try:
            start = time.time()
            b = None
            b = self.ser.write(struct.pack('<3c2B%dHB' % len(data), *total_data))
            while True:
                header = self.ser.read()
                if header == '$':
                    header = header+self.ser.read(2)
                    break
            datalength = struct.unpack('<b', self.ser.read())[0]
            code = struct.unpack('<b', self.ser.read())
            data = self.ser.read(datalength)
            temp = struct.unpack('<'+'h'*(datalength/2),data)
            self.ser.flushInput()
            self.ser.flushOutput()
            elapsed = time.time() - start
            self.attitude['angx']=float(temp[0]/10.0)
            self.attitude['angy']=float(temp[1]/10.0)
            self.attitude['heading']=float(temp[2])
            self.attitude['elapsed']=round(elapsed,3)
            self.attitude['timestamp']="%0.2f" % (time.time(),)
            return self.attitude
        except Exception as error:
            #print ("\n\nError in sendCMDreceiveATT.")
            #print ("("+str(error)+")\n\n")
            pass

    """Function to arm / disarm """
    """
    Modification required on Multiwii firmware to Protocol.cpp in evaluateCommand:

    case MSP_SET_RAW_RC:
      s_struct_w((uint8_t*)&rcSerial,16);
      rcSerialCount = 50; // 1s transition
      s_struct((uint8_t*)&att,6);
      break;
    Channel Map: AETR1234
    """
    def resetRC(self):
        timer = 0
        start = time.time()
        while timer < 500:
            data = [1500,1500,1000,1500,1000]
            self.sendCMD(10,MultiWii.SET_RAW_RC,data)
            time.sleep(50)
            timer = timer + (time.time() - start)
            start =  time.time()

    def arm(self):
        timer = 0
        start = time.time()
        while timer < 0.5:
            data = [1500,1500,1000,1500,2000]
            self.sendCMD(10,MultiWii.SET_RAW_RC,data)
            time.sleep(0.05)
            timer = timer + (time.time() - start)
            start =  time.time()

    def disarm(self):
        timer = 0
        start = time.time()
        while timer < 0.5:
            data = [1500,1500,1000,1500,1000]
            self.sendCMD(10,MultiWii.SET_RAW_RC,data)
            time.sleep(0.05)
            timer = timer + (time.time() - start)
            start =  time.time()

    def setPID(self,pd):
        nd=[]
        for i in np.arange(1,len(pd),2):
            nd.append(pd[i]+pd[i+1]*256)
        data = pd
        print ("PID sending:", data)
        self.sendCMD(30, MultiWii.SET_PID,data)
        self.sendCMD(0, MultiWii.EEPROM_WRITE,[])


    def getSaneData(self, cmd):
        print("Here will be sane data")        

    def getRangefinderData(self):
        print("getting rangefinder")        
        self.getDataV2(MultiWii.RANGEFINDER)
                
        
    def getDataV2(self, cmd):
        try:
            #start = millis()
            self.sendCMD(0, cmd, [])
            while True:
                header = self.ser.read(1)
                if header.decode('utf-8') == '$':
                    header = header+self.ser.read(2)
                    break
            print("Got header", header)
        except Exception as error:
            print (error)
            pass
            
    """Function to receive a data packet from the board"""
    def getData(self, cmd):
        try:
            start = millis()
            self.sendCMD(0,cmd,[])
            while True:
                header = self.ser.read(1)
                if header.decode() == '$':
                    header = header+self.ser.read(2)
                    break
            datalength = struct.unpack('<B', self.ser.read(1))[0]
            code = struct.unpack('<B', self.ser.read(1))[0]
            data = self.ser.read(datalength)
            #temp = struct.unpack('<'+ ('h'*int(datalength/2)) ,data)
            while self.ser.any():
                self.ser.read(1)
                
            elapsed = millis() - start
            #print("Return Code", code)
            if code == MultiWii.ATTITUDE:                
                temp = struct.unpack('<3h', data)
                self.attitude['angx']=float(temp[0]/10.0)
                self.attitude['angy']=float(temp[1]/10.0)
                self.attitude['heading']=float(temp[2])
                self.attitude['elapsed']=round(elapsed,3)
                self.attitude['timestamp']="%0.2f" % (millis(),)
                return self.attitude
            elif code == MultiWii.ALTITUDE:
                temp = struct.unpack('<ih', data)
                self.altitude['estalt']=float(temp[0])
                self.altitude['vario']=float(temp[1])
                self.altitude['elapsed']=round(elapsed,3)
                self.altitude['timestamp']="%0.2f" % (millis(),)
                return self.altitude
            elif cmd == MultiWii.RC:
                self.rcChannels['roll']=temp[0]
                self.rcChannels['pitch']=temp[1]
                self.rcChannels['yaw']=temp[2]
                self.rcChannels['throttle']=temp[3]
                self.rcChannels['elapsed']=round(elapsed,3)
                self.rcChannels['timestamp']="%0.2f" % (time.time(),)
                return self.rcChannels
            elif cmd == MultiWii.BOXNAMES:
                temp = struct.unpack('<'+ ('h'*int(datalength/2)) ,data)
                print("Boxnames", temp)
            elif cmd == MultiWii.SONARALT:
                temp = struct.unpack('<'+ ('h'*int(datalength/2)) ,data)
                print("Sonar ALT", temp)
            elif cmd == MultiWii.RAW_IMU:
                self.rawIMU['ax']=float(temp[0])
                self.rawIMU['ay']=float(temp[1])
                self.rawIMU['az']=float(temp[2])
                self.rawIMU['gx']=float(temp[3])
                self.rawIMU['gy']=float(temp[4])
                self.rawIMU['gz']=float(temp[5])
                self.rawIMU['mx']=float(temp[6])
                self.rawIMU['my']=float(temp[7])
                self.rawIMU['mz']=float(temp[8])
                self.rawIMU['elapsed']=round(elapsed,3)
                self.rawIMU['timestamp']="%0.2f" % (millis(),)
                return self.rawIMU
            elif cmd == MultiWii.ANALOG:
                self.analog['vbat']=float(temp[0])
                self.analog['intPowerMeterSum']=float(temp[1])
                self.analog['rssi']=float(temp[2])
                self.analog['amperage']=float(temp[3])
                self.analog['elapsed']=round(elapsed,3)
                self.analog['timestamp']="%0.2f" % (millis(),)
                return self.analog
            elif cmd == MultiWii.MOTOR:
                self.motor['m1']=float(temp[0])
                self.motor['m2']=float(temp[1])
                self.motor['m3']=float(temp[2])
                self.motor['m4']=float(temp[3])
                self.motor['elapsed']="%0.3f" % (elapsed,)
                self.motor['timestamp']="%0.2f" % (millis(),)
                return self.motor
            elif cmd == MultiWii.PID:
                dataPID=[]
                if len(temp)>1:
                    d=0
                    for t in temp:
                        dataPID.append(t%256)
                        dataPID.append(t/256)
                    for p in [0,3,6,9]:
                        dataPID[p]=dataPID[p]/10.0
                        dataPID[p+1]=dataPID[p+1]/1000.0
                    self.PIDcoef['rp']= dataPID=[0]
                    self.PIDcoef['ri']= dataPID=[1]
                    self.PIDcoef['rd']= dataPID=[2]
                    self.PIDcoef['pp']= dataPID=[3]
                    self.PIDcoef['pi']= dataPID=[4]
                    self.PIDcoef['pd']= dataPID=[5]
                    self.PIDcoef['yp']= dataPID=[6]
                    self.PIDcoef['yi']= dataPID=[7]
                    self.PIDcoef['yd']= dataPID=[8]
                return self.PIDcoef
            else:
                return "No return error!"
        except Exception as error:
            print (error)
            pass

    """Function to receive a data packet from the board. Note: easier to use on threads"""
    def getDataInf(self, cmd):
        while True:
            try:
                start = time.clock()
                self.sendCMD(0,cmd,[])
                while True:
                    header = self.ser.read()
                    if header == '$':
                        header = header+self.ser.read(2)
                        break
                datalength = struct.unpack('<b', self.ser.read())[0]
                code = struct.unpack('<b', self.ser.read())
                data = self.ser.read(datalength)
                temp = struct.unpack('<'+'h'*(datalength/2),data)
                elapsed = time.clock() - start
                self.ser.flushInput()
                self.ser.flushOutput()
                if cmd == MultiWii.ATTITUDE:
                    self.attitude['angx']=float(temp[0]/10.0)
                    self.attitude['angy']=float(temp[1]/10.0)
                    self.attitude['heading']=float(temp[2])
                    self.attitude['elapsed']="%0.3f" % (elapsed,)
                    self.attitude['timestamp']="%0.2f" % (time.time(),)
                elif cmd == MultiWii.RC:
                    self.rcChannels['roll']=temp[0]
                    self.rcChannels['pitch']=temp[1]
                    self.rcChannels['yaw']=temp[2]
                    self.rcChannels['throttle']=temp[3]
                    self.rcChannels['elapsed']="%0.3f" % (elapsed,)
                    self.rcChannels['timestamp']="%0.2f" % (time.time(),)
                elif cmd == MultiWii.RAW_IMU:
                    self.rawIMU['ax']=float(temp[0])
                    self.rawIMU['ay']=float(temp[1])
                    self.rawIMU['az']=float(temp[2])
                    self.rawIMU['gx']=float(temp[3])
                    self.rawIMU['gy']=float(temp[4])
                    self.rawIMU['gz']=float(temp[5])
                    self.rawIMU['elapsed']="%0.3f" % (elapsed,)
                    self.rawIMU['timestamp']="%0.2f" % (time.time(),)
                elif cmd == MultiWii.MOTOR:
                    self.motor['m1']=float(temp[0])
                    self.motor['m2']=float(temp[1])
                    self.motor['m3']=float(temp[2])
                    self.motor['m4']=float(temp[3])
                    self.motor['elapsed']="%0.3f" % (elapsed,)
                    self.motor['timestamp']="%0.2f" % (time.time(),)
            except Exception as error:
                pass

    """Function to ask for 2 fixed cmds, attitude and rc channels, and receive them. Note: is a bit slower than others"""
    def getData2cmd(self, cmd):
        try:
            start = time.time()
            self.sendCMD(0,self.ATTITUDE,[])
            while True:
                header = self.ser.read()
                if header == '$':
                    header = header+self.ser.read(2)
                    break
            datalength = struct.unpack('<b', self.ser.read())[0]
            code = struct.unpack('<b', self.ser.read())
            data = self.ser.read(datalength)
            temp = struct.unpack('<'+'h'*(datalength/2),data)
            self.ser.flushInput()
            self.ser.flushOutput()

            self.sendCMD(0,self.RC,[])
            while True:
                header = self.ser.read()
                if header == '$':
                    header = header+self.ser.read(2)
                    break
            datalength = struct.unpack('<b', self.ser.read())[0]
            code = struct.unpack('<b', self.ser.read())
            data = self.ser.read(datalength)
            temp2 = struct.unpack('<'+'h'*(datalength/2),data)
            elapsed = time.time() - start
            self.ser.flushInput()
            self.ser.flushOutput()

            if cmd == MultiWii.ATTITUDE:
                self.message['angx']=float(temp[0]/10.0)
                self.message['angy']=float(temp[1]/10.0)
                self.message['heading']=float(temp[2])
                self.message['roll']=temp2[0]
                self.message['pitch']=temp2[1]
                self.message['yaw']=temp2[2]
                self.message['throttle']=temp2[3]
                self.message['elapsed']=round(elapsed,3)
                self.message['timestamp']="%0.2f" % (time.time(),)
                return self.message
            else:
                return "No return error!"
        except Exception as error:
            print (error)
