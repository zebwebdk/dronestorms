# Quad Controller with TCP Protocol support
#
import sensor, json, image, time, network, usocket
from pyb import LED, millis

from lib.pymultiwii import MultiWii
from lib.StateMonitor import StateMonitor
from lib.Navigator import Navigator
from lib.Communicator import Communicator
from lib.mqtt import MQTTClient

# Setup Networking ## this is needed before module init
### DEV NETWORK SETTINGS
SSID='korrosion'
KEY='CorrectHorseBatteryStaple'
### IN THE WILD SETTINGS
#SSID='OnePlus5T'
#KEY='hotspot42'
# Init wlan module and connect to network
print("Trying to connect... (may take a while)...")
wlan = network.WINC()
wlan.connect(SSID, key=KEY, security=wlan.WPA_PSK)
# We should have a valid IP now via DHCP
print(wlan.ifconfig())

# Init imported modules and objects
red_led   = LED(1)
stateMonitor = StateMonitor()
msp = MultiWii()
"""
# setup TCP connection
addr = usocket.getaddrinfo("192.168.1.176", 1337)[0][4]
#addr = usocket.getaddrinfo("192.168.43.4", 1337)[0][4]
server_socket = usocket.socket(usocket.AF_INET, usocket.SOCK_DGRAM)
#server_socket.connect(addr)
server_socket.bind(addr)
server_socket.send('EY YOOO')
"""

client = MQTTClient("dronestorms", "192.168.1.176", port=1883)
client.connect()
communicator = Communicator(client)

navigator = Navigator(msp, communicator, stateMonitor)

# message handler
def handleMSG(msg):
    if msg != None and msg != '':
        msg = msg.decode('utf-8')
        print("Raw Msg", str(msg))
        if msg == '':
            return
        try:
            parsed = json.loads(msg)
            print('Decoded Command', parsed['command'])

            if parsed['command'] == 'ABORT':
                print("Disarming...")
                navigator.disarmQuad()

            if parsed['command'] == 'CODE':
                navigator.sendSafeRC()
                print('Executing Code')
                red_led.on()
                exec(parsed['code'])
                red_led.off()

        except OSError as error:
            print("MSG ERROR", error)

def OpFlowRoutine():
    img = sensor.snapshot() # Take a picture and return the image.

    displacement = extra_fb.find_displacement(img)
    extra_fb.replace(img)

    # Offset results are noisy without filtering so we drop some accuracy.
    sub_pixel_x = int(displacement.x_translation() * 5) / 5.0
    sub_pixel_y = int(displacement.y_translation() * 5) / 5.0

    print("{0:+f}x {1:+f}y {2} {3} FPS".format(sub_pixel_x, sub_pixel_y,
          displacement.response(),
          clock.fps()))

## setup sensor
sensor.reset()                      # Reset and initialize the sensor.
sensor.set_pixformat(sensor.RGB565) # Set pixel format to RGB565 (or GRAYSCALE)
sensor.set_framesize(sensor.B64X64) # Set frame size to 64x32... (or 64x64)...
sensor.skip_frames(time = 2000)     # Wait for settings take effect.
clock = time.clock()                # Create a clock object to track the FPS.

# Take from the main frame buffer's RAM to allocate a second frame buffer.
# There's a lot more RAM in the frame buffer than in the MicroPython heap.
# However, after doing this you have a lot less RAM for some algorithms...
# So, be aware that it's a lot easier to get out of RAM issues now.
extra_fb = sensor.alloc_extra_fb(sensor.width(), sensor.height(), sensor.RGB565)
extra_fb.replace(sensor.snapshot())

def checkForMessage():
    # set a timeout
    timeout = millis() + 100
    while millis() < timeout:
        server_socket.settimeout(0.1)
        msg = server_socket.recv(512)
        print(msg)
    print("MSG: ", msg)

while True:
    handleMSG(server_socket.recv(512))
    #checkForMessage()
    OpFlowRoutine()
