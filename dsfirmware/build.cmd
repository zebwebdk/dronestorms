:: clear out build dir
cd build
del /F/Q/S *.*
cd ..

:: Compile current version of lib and move it to the device
cd lib
python -m mpy_cross pymultiwii.py
python -m mpy_cross Navigator.py
python -m mpy_cross Communicator.py
python -m mpy_cross StateMonitor.py
python -m mpy_cross mqtt.py

xcopy .\__init__.py ..\build\
mv *.mpy ../build/

cd ..
rmdir -r I:\lib
mkdir I:\lib
cd build
xcopy *.* I:\lib\