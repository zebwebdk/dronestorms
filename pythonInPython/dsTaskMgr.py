
import sched, time
s = sched.scheduler(time.time, time.sleep)

def testfunc():
    print("We gots this time here", time.time())

print("started at", time.time())
s.enter(1, 1, testfunc, ())
s.enter(5, 1, testfunc, ())
s.enter(10, 1, testfunc, ())
s.enter(20, 1, testfunc, ())
s.run()

print("Stopped running at", time.time())