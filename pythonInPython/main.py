#!/usr/bin/env python2

""" main py test """

print("Importing Custom")
import customScript

custom = customScript.CustomScript()

print("init? " +str(custom.getRunning()))

print("Run Custom")
while custom.running == True:
    custom.run()
    print("Running? " +str(custom.getRunning()))

print("Custom Exitted")
print("Done...")