"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var net = __importStar(require("net"));
var server = net.createServer(function (socket) {
    socket.write('Sup Son?');
    socket.pipe(socket);
    setInterval(function () {
        socket.write('');
        socket.pipe(socket);
    }, 10000);
});
server.listen(1337, function () {
    console.log('server is up');
});
//# sourceMappingURL=tcp-server.js.map