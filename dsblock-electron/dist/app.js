"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var electron_1 = require("electron");
var main_1 = __importDefault(require("./main"));
// to handle connections and communication between the quad and the desktop app
var QuadConnectionManager_1 = require("./QuadConnectionManager");
var quadConMgr = new QuadConnectionManager_1.QuadConnectionManager();
// expose the connection manager to the desktop app
exports.getConnMgr = function () {
    return quadConMgr;
};
var ConnectionHandler_1 = require("./ConnectionHandler");
var MQTTConnectionHandler_1 = require("./MQTTConnectionHandler");
var net = __importStar(require("net"));
// var server = new net.Server()
var quadConnHandler;
var quadMQTTHandler;
var recvServer = net.createServer(function (socket) {
    quadConnHandler = new ConnectionHandler_1.ConnectionHandler(socket, quadConMgr);
});
var server = new net.Server();
server.on('connection', function (stream) {
    quadMQTTHandler = new MQTTConnectionHandler_1.MQTTConnectionHandler(stream, quadConMgr);
});
// listen for quad connection
server.listen(1337, '0.0.0.0');
recvServer.listen(1338, '0.0.0.0');
exports.commandToQuad = function (code) {
    quadMQTTHandler.sendCommand({
        command: 'CODE',
        code: code
    });
};
exports.killQuad = function () {
    quadMQTTHandler.sendCommand({
        command: 'ABORT'
    });
};
// launch electron app
main_1.default.main(electron_1.app, electron_1.BrowserWindow);
//# sourceMappingURL=app.js.map