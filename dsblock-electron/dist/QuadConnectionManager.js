"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var EventEmitter = require('events');
var QuadConnectionManager = /** @class */ (function (_super) {
    __extends(QuadConnectionManager, _super);
    function QuadConnectionManager() {
        var _this = _super.call(this) || this;
        _this.connectionStatus = false;
        return _this;
    }
    QuadConnectionManager.prototype.UpdateConnectionStatus = function (conStatus) {
        this.connectionStatus = conStatus;
        this.emit('conn_state_update', this.connectionStatus);
    };
    QuadConnectionManager.prototype.GetConnectionStatus = function () {
        return this.connectionStatus;
    };
    QuadConnectionManager.prototype.HandleData = function (data) {
        var dataStr = data.toString('utf-8');
        console.log("output:" + dataStr);
        try {
            if (dataStr.indexOf('output') > -1) {
                this.emit('quad_message', JSON.parse(dataStr));
            }
            else if (dataStr.indexOf('sticks') > -1) {
                console.log('sticks', dataStr);
                this.emit('quad_sticks', JSON.parse(dataStr));
            }
        }
        catch (e) {
            console.error(e, dataStr);
        }
    };
    return QuadConnectionManager;
}(EventEmitter));
exports.QuadConnectionManager = QuadConnectionManager;
//# sourceMappingURL=QuadConnectionManager.js.map