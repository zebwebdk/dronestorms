"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mqtt_connection_1 = __importDefault(require("mqtt-connection"));
var MQTTConnectionHandler = /** @class */ (function () {
    function MQTTConnectionHandler(socket, connMgr) {
        this.connection = mqtt_connection_1.default(socket);
        this.connMgr = connMgr;
        this.socket = socket;
        this.bindEvents();
    }
    MQTTConnectionHandler.prototype.sendCommand = function (command) {
        console.log('command', command);
        var cmdObj = {};
        cmdObj.topic = command.command;
        if (command.code) {
            cmdObj.payload = command.code;
        }
        this.connection.publish(cmdObj);
    };
    MQTTConnectionHandler.prototype.bindEvents = function () {
        var _this = this;
        this.connection.on('connect', function (packet) {
            console.log('Client connected', packet);
            _this.connection.connack({ returnCode: 0 });
            _this.connMgr.UpdateConnectionStatus(true);
        });
        this.connection.on('publish', function (packet) {
            console.log('client published', packet);
            _this.connection.puback({
                messageId: packet.messageId
            });
        });
        this.connection.on('subscribe', function (packet) {
            // send a suback with messageId and granted QoS level
            _this.connection.suback({ granted: [packet.qos], messageId: packet.messageId });
        });
        this.connection.on('pingreq', function () {
            _this.connection.pingresp();
            _this.connMgr.UpdateConnectionStatus(true);
        });
        this.connection.on('close', function () {
            console.log('Connection Closed');
            _this.connection.destroy();
            _this.connMgr.UpdateConnectionStatus(false);
        });
        this.connection.on('error', function () {
            console.log('Connection Error');
            _this.connection.destroy();
            _this.connMgr.UpdateConnectionStatus(false);
        });
        this.connection.on('disconnect', function () {
            console.log('Connection Disconnected');
            _this.connection.destroy();
            _this.connMgr.UpdateConnectionStatus(false);
        });
        // socket timer ud efter 15 minutter 
        this.socket.setTimeout(1000 * 60 * 15);
        this.socket.on('timeout', function () {
            console.log('Socket Timed out');
            _this.connection.destroy();
            _this.connMgr.UpdateConnectionStatus(false);
        });
    };
    return MQTTConnectionHandler;
}());
exports.MQTTConnectionHandler = MQTTConnectionHandler;
//# sourceMappingURL=MQTTConnectionHandler.js.map