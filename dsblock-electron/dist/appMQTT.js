"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var electron_1 = require("electron");
var main_1 = __importDefault(require("./main"));
/* stub listener class thing */
var EventEmitter = require('events');
var QuadConnectionManager = /** @class */ (function (_super) {
    __extends(QuadConnectionManager, _super);
    function QuadConnectionManager() {
        var _this = _super.call(this) || this;
        _this.connectionStatus = false;
        return _this;
    }
    QuadConnectionManager.prototype.UpdateConnectionStatus = function (conStatus) {
        this.connectionStatus = conStatus;
        this.emit('conn_state_update', this.connectionStatus);
    };
    QuadConnectionManager.prototype.GetConnectionStatus = function () {
        return this.connectionStatus;
    };
    return QuadConnectionManager;
}(EventEmitter));
exports.QuadConnectionManager = QuadConnectionManager;
require('electron-reload')(__dirname);
var net = require('net');
var mqttCon = require('mqtt-connection');
var server = new net.Server();
var quadConMgr = new QuadConnectionManager();
var quadConnection;
exports.getConnMgr = function () {
    return quadConMgr;
};
server.on('connection', function (stream) {
    var client = mqttCon(stream);
    // client connected
    client.on('connect', function (packet) {
        // acknowledge the connect packet
        client.connack({ returnCode: 0 });
        console.log('Client Connected');
        if (client.options.clientId == 'DSQuad') {
            console.log('QuadConnect!');
            quadConMgr.UpdateConnectionStatus(true);
            quadConnection = client;
        }
    });
    // client published
    client.on('publish', function (packet) {
        // send a puback with messageId (for QoS > 0)
        // client.puback({ messageId: packet.messageId })
        console.log('Client Published', packet);
        var buffer = packet.payload;
        console.log('String?', buffer.toString());
    });
    // client pinged
    client.on('pingreq', function () {
        // send a pingresp
        client.pingresp();
    });
    // client subscribed
    client.on('subscribe', function (packet) {
        // send a suback with messageId and granted QoS level
        client.suback({ granted: [packet.qos], messageId: packet.messageId });
    });
    // timeout idle streams after 5 minutes
    stream.setTimeout(1000 * 60 * 5);
    // connection error handling
    client.on('close', function () {
        console.log('Connection closed to', client.options.clientId);
        if (client.options.clientId == 'DSQuad') {
            quadConMgr.UpdateConnectionStatus(false);
        }
        client.destroy();
    });
    client.on('error', function () {
        if (client.options.clientId == 'DSQuad') {
            quadConMgr.UpdateConnectionStatus(false);
        }
        client.destroy();
    });
    client.on('disconnect', function () {
        console.log('Client Disconnected');
        if (client.options.clientId == 'DSQuad') {
            quadConMgr.UpdateConnectionStatus(false);
        }
        client.destroy();
    });
    // stream timeout
    stream.on('timeout', function () {
        console.log('Stream timed out');
        if (client.options.clientId == 'DSQuad') {
            quadConMgr.UpdateConnectionStatus(false);
        }
        client.destroy();
    });
});
// listen on port 1883
server.listen(1883, function () {
    console.log('MQTT Connection listening on 1883');
});
exports.commandToQuad = function (code) {
    if (quadConnection == null) {
        return;
    }
    console.log('Pushing command to quad');
    quadConnection.publish({
        topic: 'dronestorms/betaQuad',
        payload: code
    });
};
main_1.default.main(electron_1.app, electron_1.BrowserWindow);
//# sourceMappingURL=appMQTT.js.map