"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var electron_1 = require("electron");
var main_1 = __importDefault(require("./main"));
// to handle connections and communication between the quad and the desktop app
var QuadConnectionManager_1 = require("./QuadConnectionManager");
var quadConMgr = new QuadConnectionManager_1.QuadConnectionManager();
// expose the connection manager to the desktop app
exports.getConnMgr = function () {
    return quadConMgr;
};
var ConnectionHandler_1 = require("./ConnectionHandler");
var net = __importStar(require("net"));
var quadConnHandler;
var server = net.createServer(function (socket) {
    quadConnHandler = new ConnectionHandler_1.ConnectionHandler(socket, quadConMgr);
});
// listen for quad connection
server.listen(1337, '0.0.0.0');
exports.commandToQuad = function (code) {
    quadConnHandler.sendCommand({
        command: 'CODE',
        code: code
    });
};
exports.killQuad = function () {
    quadConnHandler.sendCommand({
        command: 'ABORT'
    });
};
// launch electron app
main_1.default.main(electron_1.app, electron_1.BrowserWindow);
//# sourceMappingURL=appTCP.js.map