"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ConnectionHandler = /** @class */ (function () {
    function ConnectionHandler(socket, connMgr) {
        this.connectedSocket = socket;
        this.connMgr = connMgr;
        this.setupConnection();
    }
    ConnectionHandler.prototype.setupConnection = function () {
        var _this = this;
        var welcomeMessage = {
            command: 'WELCOME'
        };
        this.sendCommand(welcomeMessage);
        this.connMgr.UpdateConnectionStatus(true);
        // bind events
        this.connectedSocket.on('error', function (err) {
            if (err.message === 'read ECONNRESET') {
                console.error('CH: Client Disconnected --> Connection reset');
            }
            else {
                console.log('CH:', 'ERROR', err);
            }
            _this.connMgr.UpdateConnectionStatus(false);
        });
        this.connectedSocket.on('data', function (data) {
            _this.connMgr.HandleData(data);
        });
    };
    ConnectionHandler.prototype.sendCommand = function (command) {
        if (this.connectedSocket) {
            this.connectedSocket.write(JSON.stringify(command));
        }
    };
    return ConnectionHandler;
}());
exports.ConnectionHandler = ConnectionHandler;
//# sourceMappingURL=ConnectionHandler.js.map