const electron = require('electron');
const main = electron.remote.require('./main').TestFunc;
const connMgr = electron.remote.require('./app').getConnMgr();
const commandToQuad = electron.remote.require('./app').commandToQuad;

connMgr.on('conn_state_update', (connStatus) => {
    const conStatusFooter = document.getElementById('connectionStatusFooter');
    if(connStatus === true) {                
        conStatusFooter.innerHTML = '<span style="color: green;">CONNECTED</span>';
    } else {
        conStatusFooter.innerHTML = '<span style="color: red;">DISCONNECTED</span>';
    }
    console.log('Connection Status Updated', connStatus);
});

console.log('Got Connection Manager', connMgr);

const runBtn = document.querySelector('.runBtn');
runBtn.addEventListener('click', () => {
    console.log('run btn pressed');
    const code = Blockly.Python.workspaceToCode(workspace);
    commandToQuad(code);
});

/* BLOCKLY STUFF */

var toolbox = document.getElementById("toolbox");

var options = { 
    toolbox : toolbox, 
    collapse : false, 
    comments : false, 
    disable : false, 
    maxBlocks : Infinity, 
    trashcan : false, 
    horizontalLayout : false, 
    toolboxPosition : 'start', 
    css : true, 
    media : 'https://blockly-demo.appspot.com/static/media/', 
    rtl : false, 
    scrollbars : false, 
    sounds : true, 
    oneBasedIndex : true
};

var workspace = Blockly.inject('blocklyDiv', options);

var workspaceBlocks = document.getElementById("workspaceBlocks"); 

Blockly.Xml.domToWorkspace(workspaceBlocks, workspace);

const updateCodePreview = () => {
    const code = Blockly.Python.workspaceToCode(workspace);
    console.log('Code', code);            
    const previewElement = document.getElementById('codePreview');
    const previewCode = '\n' + code + '\n';
    previewElement.innerHTML = previewCode;            
    Prism.highlightElement(previewElement);
};

workspace.addChangeListener(updateCodePreview);