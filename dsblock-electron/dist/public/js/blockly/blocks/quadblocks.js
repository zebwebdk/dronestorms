Blockly.defineBlocksWithJsonArray([{
  "type": "arm_quad",
  "message0": "Arm Quadcopter",
  "nextStatement": null,
  "colour": 0,
  "tooltip": "Send ARM Command to Quadcopter",
  "helpUrl": ""
},
{
  "type": "quad_rise",
  "message0": "Quad Higher %1 Time %2 %3",
  "args0": [
    {
      "type": "input_dummy"
    },
    {
      "type": "field_number",
      "name": "seconds",
      "value": 5,
      "min": 0
    },
    {
      "type": "input_value",
      "name": "NAME"
    }
  ],
  "previousStatement": null,
  "nextStatement": null,
  "colour": 230,
  "tooltip": "",
  "helpUrl": ""
},
{
  "type": "quad_fall",
  "message0": "Quad Lower %1 Time %2 %3",
  "args0": [
    {
      "type": "input_dummy"
    },
    {
      "type": "field_number",
      "name": "seconds",
      "value": 0
    },
    {
      "type": "input_value",
      "name": "NAME"
    }
  ],
  "previousStatement": null,
  "nextStatement": null,
  "colour": 230,
  "tooltip": "Lower the quad altitude",
  "helpUrl": ""
},
{
  "type": "quad_left",
  "message0": "Quad Left %1 Time %2 %3",
  "args0": [
    {
      "type": "input_dummy"
    },
    {
      "type": "field_number",
      "name": "seconds",
      "value": 5,
      "min": 0
    },
    {
      "type": "input_value",
      "name": "NAME",
      "check": "Number"
    }
  ],
  "previousStatement": null,
  "nextStatement": null,
  "colour": 135,
  "tooltip": "Navigate Quadcopter Left",
  "helpUrl": ""
},
{
  "type": "quad_right",
  "message0": "Quad Right %1 Time %2 %3",
  "args0": [
    {
      "type": "input_dummy"
    },
    {
      "type": "field_number",
      "name": "seconds",
      "value": 5,
      "min": 0
    },
    {
      "type": "input_value",
      "name": "NAME",
      "check": "Number"
    }
  ],
  "previousStatement": null,
  "nextStatement": null,
  "colour": 135,
  "tooltip": "Navigate Quadcopter Right",
  "helpUrl": ""
},
{
  "type": "quad_fwd",
  "message0": "Quad Forward %1 Time %2 %3",
  "args0": [
    {
      "type": "input_dummy"
    },
    {
      "type": "field_number",
      "name": "seconds",
      "value": 5,
      "min": 0
    },
    {
      "type": "input_value",
      "name": "NAME"
    }
  ],
  "previousStatement": null,
  "nextStatement": null,
  "colour": 135,
  "tooltip": "",
  "helpUrl": ""
},
{
  "type": "quad_hover",
  "message0": "Quad Hover %1 %2 %3",
  "args0": [
    {
      "type": "input_dummy"
    },
    {
      "type": "field_number",
      "name": "seconds",
      "value": 5,
      "min": 0
    },
    {
      "type": "input_value",
      "name": "NAME"
    }
  ],
  "previousStatement": null,
  "nextStatement": null,
  "colour": 135,
  "tooltip": "",
  "helpUrl": ""
},
{
  "type": "quad_back",
  "message0": "Quad Back %1 Time %2 %3",
  "args0": [
    {
      "type": "input_dummy"
    },
    {
      "type": "field_number",
      "name": "seconds",
      "value": 5,
      "min": 0
    },
    {
      "type": "input_value",
      "name": "NAME"
    }
  ],
  "previousStatement": null,
  "nextStatement": null,
  "colour": 135,
  "tooltip": "",
  "helpUrl": ""
},
{
  "type": "quad_sethead",
  "message0": "Quad Heading %1 %2 %3",
  "args0": [
    {
      "type": "input_dummy"
    },
    {
      "type": "field_number",
      "name": "HEADING",
      "value": 5,
      "min": 0
    },
    {
      "type": "input_value",
      "name": "NAME"
    }
  ],
  "previousStatement": null,
  "nextStatement": null,
  "colour": 135,
  "tooltip": "",
  "helpUrl": ""
},
{
  "type": "quad_disarm",
  "message0": "Disarm Quad",
  "previousStatement": null,
  "colour": 0,
  "tooltip": "Disarm Quad",
  "helpUrl": ""
},
{
  "type": "quad_sensor_altitude",
  "message0": "Sensor: Altitude",
  "output": null,
  "colour": 270,
  "tooltip": "Read Altitude from Quad",
  "helpUrl": ""
},
{
  "type": "quad_attitude",
  "message0": "Quad Attitude",
  "output": null,
  "colour": 270,
  "tooltip": "Read Current Attitude from Quad",
  "helpUrl": ""
},
{
  "type": "quad_doUntil",
  "message0": "for %1 second(s)",
  "args0": [{
    "type": "field_number",
    "name": "SECONDS",
    "value": 10,
    "min": 1,
    "precision": 0.1
  }],
  "message1": "repeat %1",
  "args1": [{
    "type": "input_statement",
    "name": "DO"
  }],
  "previousStatement": null,
  "nextStatement": null,
  "style": "loop_blocks",
  "helpUrl": "",  
}
]);
