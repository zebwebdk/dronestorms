Blockly.Python['arm_quad'] = function(block) {
  // TODO: Assemble Python into code variable.
  var code = `navigator.armQuad()\n`;
  return code;
};

Blockly.Python['quad_rise'] = function(block) {
  var number_seconds = block.getFieldValue('seconds');
  var value_name = Blockly.Python.valueToCode(block, 'NAME', Blockly.Python.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  // var code = `print("Quad is rising for", ${number_seconds}, "seconds")\n`;
  code = 'navigator.raiseQuad(' + number_seconds + ')\n';
  return code;
};

Blockly.Python['quad_fall'] = function(block) {
  var number_seconds = block.getFieldValue('seconds');
  var value_name = Blockly.Python.valueToCode(block, 'NAME', Blockly.Python.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  // var code = `print("Quad is falling for", ${number_seconds}, "seconds")\n`;
  code = 'navigator.lowerQuad(' + number_seconds + ')\n';
  return code;
};

Blockly.Python['quad_left'] = function(block) {
  var number_seconds = block.getFieldValue('seconds');
  var value_name = Blockly.Python.valueToCode(block, 'NAME', Blockly.Python.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  var code = `print("Quad is navigating left for", ${number_seconds}, "seconds")\n`;
  return code;
};

Blockly.Python['quad_right'] = function(block) {
  var number_seconds = block.getFieldValue('seconds');
  var value_name = Blockly.Python.valueToCode(block, 'NAME', Blockly.Python.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  var code = `print("Quad is navigating right for", ${number_seconds}, "seconds")\n`;
  return code;
};

Blockly.Python['quad_fwd'] = function(block) {
  var number_seconds = block.getFieldValue('seconds');
  var value_name = Blockly.Python.valueToCode(block, 'NAME', Blockly.Python.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  var code = `print("Quad is navigating forward for", ${number_seconds}, "seconds")\n`;
  return code;
};

Blockly.Python['quad_back'] = function(block) {
  var number_seconds = block.getFieldValue('seconds');
  var value_name = Blockly.Python.valueToCode(block, 'NAME', Blockly.Python.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  var code = `print("Quad is navigating backwards for", ${number_seconds}, "seconds")\n`;
  return code;
};

Blockly.Python['quad_disarm'] = function(block) {
  // TODO: Assemble Python into code variable.
  var code = `navigator.disarmQuad()\n`;
  return code;
};

Blockly.Python['quad_hover'] = function(block) {
  var number_seconds = block.getFieldValue('seconds');
  var value_name = Blockly.Python.valueToCode(block, 'NAME', Blockly.Python.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  // var code = `print("Quad is falling for", ${number_seconds}, "seconds")\n`;
  code = 'hoverQuad(' + number_seconds + ')\n';
  return code;
};

Blockly.Python['quad_sensor_altitude'] = function(block) {  
  return 'getQuadAltitude()\n';
};

Blockly.Python['quad_attitude'] = function(block) {
  return 'getQuadAttitude()\n';
}

Blockly.Python['quad_doUntil'] = function(block) {  
  var seconds = block.getFieldValue('SECONDS');
  var secToMillis = parseFloat(seconds) * 1000;
  console.log(seconds, secToMillis);
  var loopCode = Blockly.Python.statementToCode(block, 'DO');
  console.log('LoopCode', loopCode);
  loopCode = Blockly.Python.addLoopTrap(loopCode, block.id) || Blockly.Python.PASS;
  var code = `startTime = millis() + ${secToMillis}:\n${loopCode}`;
  return code;
}

Blockly.Python['quad_sethead'] = function(block) {
  var heading = block.getFieldValue('HEADING');
  return 'setHead(' + heading + ')\n';
}
