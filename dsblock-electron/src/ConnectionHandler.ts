import { Socket } from 'net';
import { QuadConnectionManager } from './QuadConnectionManager';


export class ConnectionHandler {

    private connectedSocket: Socket;
    private connMgr: QuadConnectionManager;

    constructor(socket: Socket, connMgr: QuadConnectionManager) {
        this.connectedSocket = socket;
        this.connMgr = connMgr;
        this.setupConnection();
    }

    private setupConnection() {
        const welcomeMessage = {
            command: 'WELCOME'    
        }
        this.sendCommand(welcomeMessage);
        this.connMgr.UpdateConnectionStatus(true);


        // bind events
        this.connectedSocket.on('error', (err) => {
            if (err.message === 'read ECONNRESET') {
                console.error('CH: Client Disconnected --> Connection reset');                
            } else {
                console.log('CH:', 'ERROR', err);
            }

            this.connMgr.UpdateConnectionStatus(false);
        })

        this.connectedSocket.on('data', (data: Buffer) => {
            this.connMgr.HandleData(data);
        });
    }

    public sendCommand(command) {
        if (this.connectedSocket) {
            this.connectedSocket.write(JSON.stringify(command));
        }
    }

}