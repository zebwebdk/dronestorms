import { app, BrowserWindow } from 'electron';
import Main from './main';

/* stub listener class thing */
const EventEmitter = require('events');
export class QuadConnectionManager extends EventEmitter {
    
    private connectionStatus;

    constructor() {
        super();        
        this.connectionStatus = false;
    }

    UpdateConnectionStatus(conStatus: boolean) {
        this.connectionStatus = conStatus;
        this.emit('conn_state_update', this.connectionStatus);
    }

    GetConnectionStatus() {
        return this.connectionStatus;
    }
}


require('electron-reload')(__dirname);

var net = require('net')
var mqttCon = require('mqtt-connection')
var server = new net.Server()
var quadConMgr = new QuadConnectionManager();
var quadConnection;

export const getConnMgr = () => {
    return quadConMgr;
}

server.on('connection', function (stream) {
    var client = mqttCon(stream)    
    
    // client connected
    client.on('connect', function (packet) {
      // acknowledge the connect packet
      client.connack({ returnCode: 0 });            
      console.log('Client Connected');
      if(client.options.clientId == 'DSQuad') {
        console.log('QuadConnect!');
        quadConMgr.UpdateConnectionStatus(true);       
        quadConnection = client; 
      }
    })
   
    // client published
    client.on('publish', function (packet) {
      // send a puback with messageId (for QoS > 0)
      // client.puback({ messageId: packet.messageId })
      console.log('Client Published', packet);
      const buffer = packet.payload;
      console.log('String?', buffer.toString());
    })
   
    // client pinged
    client.on('pingreq', function () {
      // send a pingresp
      client.pingresp()
    });
   
    // client subscribed
    client.on('subscribe', function (packet) {
      // send a suback with messageId and granted QoS level
      client.suback({ granted: [packet.qos], messageId: packet.messageId })
    })
   
    // timeout idle streams after 5 minutes
    stream.setTimeout(1000 * 60 * 5)
   
    // connection error handling
    client.on('close', function () { 
        console.log('Connection closed to', client.options.clientId);
        if(client.options.clientId == 'DSQuad') {
            quadConMgr.UpdateConnectionStatus(false);
        }
        client.destroy() })

    client.on('error', function () { 
        if(client.options.clientId == 'DSQuad') {
            quadConMgr.UpdateConnectionStatus(false);
        }
        client.destroy() })

    client.on('disconnect', function () { 
        console.log('Client Disconnected');
        if(client.options.clientId == 'DSQuad') {
            quadConMgr.UpdateConnectionStatus(false);
        }
        client.destroy() })
   
    // stream timeout
    stream.on('timeout', function () { 
        console.log('Stream timed out');
        if(client.options.clientId == 'DSQuad') {
            quadConMgr.UpdateConnectionStatus(false);
        }
        client.destroy(); })
  })
   
  // listen on port 1883
  server.listen(1883, () => {
    console.log('MQTT Connection listening on 1883')    
  });

export const commandToQuad = (code) => {
    if(quadConnection == null) {
        return;    
    }
    console.log('Pushing command to quad');
    quadConnection.publish({
        topic: 'dronestorms/betaQuad',
        payload: code
    });

}

Main.main(app, BrowserWindow);
