const electron = require('electron');
const main = electron.remote.require('./main').TestFunc;
const connMgr = electron.remote.require('./app').getConnMgr();
const commandToQuad = electron.remote.require('./app').commandToQuad;
const killQuad = electron.remote.require('./app').killQuad;

connMgr.on('conn_state_update', (connStatus) => {
    const conStatusFooter = document.getElementById('connectionStatusFooter');
    if(connStatus === true) {                
        conStatusFooter.innerHTML = '<span style="color: green;">CONNECTED</span>';
    } else {
        conStatusFooter.innerHTML = '<span style="color: red;">DISCONNECTED</span>';
    }
    console.log('Connection Status Updated', connStatus);
});

connMgr.on('quad_message', (quadMsg) => {
    const quadConsole = document.getElementById('quadConsole');
    quadConsole.append(" " + quadMsg.output + '\n');    
    quadConsole.scrollTop = quadConsole.scrollHeight;
});

connMgr.on('quad_sticks', (quadSticks) => {
    console.log('QuadSticks', quadSticks);
    drawSticks(quadSticks.a, quadSticks.e, quadSticks.t, quadSticks.r);
})

console.log('Got Connection Manager', connMgr);

const runBtn = document.querySelector('.runBtn');
runBtn.addEventListener('click', () => {
    console.log('run btn pressed');
    const code = Blockly.Python.workspaceToCode(workspace);
    commandToQuad(code);
});

const abortBtn = document.querySelector('.abortBtn');
abortBtn.addEventListener('click', () => {
    console.log('abort was pressed');
    killQuad();
});

/* BLOCKLY STUFF */

var toolbox = document.getElementById("toolbox");

var options = { 
    toolbox : toolbox, 
    collapse : false, 
    comments : false, 
    disable : false, 
    maxBlocks : Infinity, 
    trashcan : false, 
    horizontalLayout : false, 
    toolboxPosition : 'start', 
    css : true, 
    media : 'https://blockly-demo.appspot.com/static/media/', 
    rtl : false, 
    scrollbars : false, 
    sounds : true, 
    oneBasedIndex : true
};

var workspace = Blockly.inject('blocklyDiv', options);

var workspaceBlocks = document.getElementById("workspaceBlocks"); 

Blockly.Xml.domToWorkspace(workspaceBlocks, workspace);

const updateCodePreview = () => {
    const code = Blockly.Python.workspaceToCode(workspace);
    console.log('Code', code);            
    const previewElement = document.getElementById('codePreview');
    const previewCode = '\n' + code + '\n';
    previewElement.innerHTML = previewCode;            
    Prism.highlightElement(previewElement);
};

workspace.addChangeListener(updateCodePreview);

// set stick output
const leftStick = document.getElementById("leftStick").getContext('2d');
const rightStick = document.getElementById("rightStick").getContext('2d');
const setupSticks = () => {    

    leftStick.beginPath();
    leftStick.moveTo(0, 50);
    leftStick.lineTo(100, 50);
    leftStick.moveTo(50, 0);
    leftStick.lineTo(50, 100);    
    leftStick.closePath();

    leftStick.fill();
    leftStick.strokeStyle = 'black';
    leftStick.stroke();
    
    leftStick.save();

    rightStick.beginPath();
    rightStick.moveTo(0, 50);
    rightStick.lineTo(100, 50);
    rightStick.moveTo(50, 0);
    rightStick.lineTo(50, 100);    
    rightStick.closePath();
    rightStick.fill();
    rightStick.strokeStyle = 'black';
    rightStick.stroke();
    rightStick.save();
}

const drawSticks = (a, e, t, r) => {        
    // normalize vores inputs til at passe over 100 units
    // dvs. vi trækker 1000 fra og dividerer med 10
    a = ((a - 1000) / 10);    
    e = (((e - 1000) / 10) * -1) + 100;
    t = (((t - 1000) / 10) * -1) + 100;
    r = ((r - 1000) / 10);

    console.log('aetr', a, e, t, r);

    leftStick.clearRect(0, 0, 100, 100);
    rightStick.clearRect(0, 0, 100, 100);

    leftStick.restore();
    rightStick.restore();

    setupSticks();

    leftStick.moveTo(50, 50);
    leftStick.beginPath();
    leftStick.arc(r, t, 15, 0, Math.PI * 2);
    leftStick.closePath();
    leftStick.fillStyle = 'rgba(192, 0, 0, 0.7)';
    leftStick.fill();
    leftStick.stroke();
    
    rightStick.moveTo(50, 50)
    rightStick.beginPath();
    rightStick.arc(a, e, 15, 0, Math.PI * 2);
    rightStick.closePath();
    leftStick.fillStyle = 'rgba(192, 0, 0, 0.7)';
    rightStick.fill();
    rightStick.stroke();

}

setupSticks();
drawSticks(1500, 1500, 1000, 1500);
