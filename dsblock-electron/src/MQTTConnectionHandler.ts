import { Socket } from 'net';
import mqttCon from 'mqtt-connection';
import { QuadConnectionManager } from './QuadConnectionManager';

export class MQTTConnectionHandler {
    // socket: Socket;    
    connection: any;
    socket: Socket;
    connMgr: QuadConnectionManager;
    constructor(socket: Socket, connMgr: QuadConnectionManager) {
        this.connection = mqttCon(socket);
        this.connMgr = connMgr;
        this.socket = socket;
        this.bindEvents();
    }

    sendCommand(command) {
        console.log('command', command);
        const cmdObj: any = {};
        cmdObj.topic = command.command;
        if(command.code) {
            cmdObj.payload = command.code;
        }
        this.connection.publish(cmdObj);
    }

    bindEvents() {
        this.connection.on('connect', (packet) => {
            console.log('Client connected', packet);
            this.connection.connack({ returnCode: 0 });
            this.connMgr.UpdateConnectionStatus(true);
        });

        this.connection.on('publish', (packet) => {
            console.log('client published', packet);
            this.connection.puback({
                messageId: packet.messageId
            });
        });

        this.connection.on('subscribe', (packet) => {
            // send a suback with messageId and granted QoS level
            this.connection.suback({ granted: [packet.qos], messageId: packet.messageId })
        });

        this.connection.on('pingreq', () => {            
            this.connection.pingresp();
            this.connMgr.UpdateConnectionStatus(true);
        });

        this.connection.on('close', () => {
            console.log('Connection Closed');
            this.connection.destroy();
            this.connMgr.UpdateConnectionStatus(false);
        });
        this.connection.on('error', () => {
            console.log('Connection Error');
            this.connection.destroy();
            this.connMgr.UpdateConnectionStatus(false);
        });
        this.connection.on('disconnect', () => {
            console.log('Connection Disconnected');
            this.connection.destroy();
            this.connMgr.UpdateConnectionStatus(false);
        });
        // socket timer ud efter 15 minutter 
        this.socket.setTimeout(1000 * 60 * 15);
        this.socket.on('timeout', () => {
            console.log('Socket Timed out');
            this.connection.destroy();
            this.connMgr.UpdateConnectionStatus(false);
        })
    }


}