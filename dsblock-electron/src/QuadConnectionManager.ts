const EventEmitter = require('events');
export class QuadConnectionManager extends EventEmitter {
    
    private connectionStatus;

    constructor() {
        super();        
        this.connectionStatus = false;
    }

    UpdateConnectionStatus(conStatus: boolean) {
        this.connectionStatus = conStatus;
        this.emit('conn_state_update', this.connectionStatus);
    }

    GetConnectionStatus() {
        return this.connectionStatus;
    }

    HandleData(data: Buffer) {
        const dataStr = data.toString('utf-8');
        console.log("output:"+dataStr);
        try {
            if(dataStr.indexOf('output') > -1) {
                this.emit('quad_message', JSON.parse(dataStr));
            } else if(dataStr.indexOf('sticks') > -1) {
                console.log('sticks', dataStr);
                this.emit('quad_sticks', JSON.parse(dataStr));
            }
        } catch (e) {
            console.error(e, dataStr);
        }
    }
}