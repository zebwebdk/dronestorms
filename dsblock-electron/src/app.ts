import { app, BrowserWindow } from 'electron';
import Main from './main';
// to handle connections and communication between the quad and the desktop app
import { QuadConnectionManager } from './QuadConnectionManager';
const quadConMgr = new QuadConnectionManager();
// expose the connection manager to the desktop app
export const getConnMgr = () => {
    return quadConMgr;
}

import { ConnectionHandler } from './ConnectionHandler';
import { MQTTConnectionHandler } from './MQTTConnectionHandler';

import * as net from 'net';

// var server = new net.Server()

let quadConnHandler: ConnectionHandler;
let quadMQTTHandler: MQTTConnectionHandler;
const recvServer = net.createServer((socket) => {
    quadConnHandler = new ConnectionHandler(socket, quadConMgr);
});
const server = new net.Server()
server.on('connection', (stream: net.Socket) => {
    quadMQTTHandler = new MQTTConnectionHandler(stream, quadConMgr);
});

// listen for quad connection
server.listen(1337, '0.0.0.0');
recvServer.listen(1338, '0.0.0.0');

export const commandToQuad = (code) => {    
    quadMQTTHandler.sendCommand({
        command: 'CODE',
        code
    });
}

export const killQuad = () => {
    quadMQTTHandler.sendCommand({
        command: 'ABORT'
    });
}

// launch electron app
Main.main(app, BrowserWindow);
