import { app, BrowserWindow } from 'electron';
import Main from './main';
// to handle connections and communication between the quad and the desktop app
import { QuadConnectionManager } from './QuadConnectionManager';
const quadConMgr = new QuadConnectionManager();
// expose the connection manager to the desktop app
export const getConnMgr = () => {
    return quadConMgr;
}

import { ConnectionHandler } from './ConnectionHandler';
import * as net from 'net';

let quadConnHandler: ConnectionHandler;

const server = net.createServer((socket) => {
    quadConnHandler = new ConnectionHandler(socket, quadConMgr);
});

// listen for quad connection
server.listen(1337, '0.0.0.0');

export const commandToQuad = (code) => {    
    quadConnHandler.sendCommand({
        command: 'CODE',
        code
    });
}

export const killQuad = () => {
    quadConnHandler.sendCommand({
        command: 'ABORT'
    });
}

// launch electron app
Main.main(app, BrowserWindow);
