import * as net from 'net';

const server = net.createServer((socket) => {
    socket.write('Sup Son?');
    socket.pipe(socket);

    setInterval(() => {
        socket.write('');   
        socket.pipe(socket);
    }, 10000);
});

server.listen(1337, () => {
    console.log('server is up');
});