from flask import Flask, Response
import cv2
from math import sqrt
import apriltag
import numpy as np

class VideoStreamPasser():

    def __init__(self, drone, shouldUseCV, moveToTag, setMoveToTag, setFakeResponse, setDetectedTags):
        print("Video Stream Passthrough Initiated")
        #self.drone = cv2.VideoCapture(0)
        self.drone = drone
        self.moveToTag = moveToTag
        self.setDetectedTags = setDetectedTags
        self.setFakeResponse = setFakeResponse
        self.setMoveToTag = setMoveToTag
        self.useCV = shouldUseCV
        self.detector = apriltag.Detector()
        self.webserver = Flask(__name__)
        self.bindRoutes()
        self.last_frame = None
        self.webserver.run(
            port=5001
        )
        
    def distanceBetweenPoints(self, p1, p2):    
        return sqrt(((p1[0]-p2[0])**2)+((p1[1]-p2[1])**2))

    def pixel2mmRatio(self, pixels, mm):
        return mm / pixels

    def serveVideo(self):
        while self.streaming:
            frame = self.drone.read()        

            if frame is None or frame.size == 0:
                print("No frame")
                continue 
            
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)                        
            if self.useCV() == True:                
                colorImage = cv2.cvtColor(gray, cv2.COLOR_GRAY2RGB)
                tags = self.detector.detect(gray)
                self.setDetectedTags(tags)
                for tag in tags:
                    #print("Tag", tag.tag_id)
                    pts = np.array([tag.corners[0], tag.corners[1], tag.corners[2], tag.corners[3]], np.int32)
                    cv2.polylines(colorImage,[pts],True,(0,0,255), 4)
                    if self.moveToTag()[0]:
                        centerX = 460
                        centerY = 360
                        print("Moving to tag", self.moveToTag()[1])
                        if tag.tag_id == self.moveToTag()[1]:
                            # color as primary tag
                            cv2.polylines(colorImage,[pts],True,(0,255,0), 4)
                            # ratio and distance
                            print(tag.corners[0], tag.corners[1])
                            dist = self.distanceBetweenPoints(tag.corners[0], tag.corners[1])
                            pixRatio = self.pixel2mmRatio(dist, 55)
                            distanceToCenter = self.distanceBetweenPoints(tag.center, (centerX, centerY)) * pixRatio     
                            cv2.line(colorImage, (centerX, centerY), (int(tag.center[0]), int(tag.center[1])), (0, 255, 0), 2)
                            xMove = int(((tag.center[0]- centerX) * pixRatio) / 10)
                            yMove = int(((centerY - tag.center[1]) * pixRatio) / 10)
                            moveCMD = "rc " + str(xMove) + " " + str(yMove) + " 0 0"
                            self.drone.send_command(moveCMD)
                            print("Distance To Center", distanceToCenter)
                            #deadzone stuff...
                            if distanceToCenter < 100:
                                self.drone.send_command('rc 0 0 0 0')
                                #time.sleep(5)
                                self.setMoveToTag(False, None)                                
                                self.setFakeResponse()

                cvRet, cvImage = cv2.imencode('.jpg', colorImage)

                #cv2.imshow('colorimage', colorImage)
                #cv2.waitKey(1)
                yield (b'--frame\r\n'
                    b'Content-Type: image/jpeg\r\n\r\n' + cvImage.tobytes() + b'\r\n')
            else:                
                ret, image = cv2.imencode('.jpg', gray)            
                yield (b'--frame\r\n'
                    b'Content-Type: image/jpeg\r\n\r\n' + image.tobytes() + b'\r\n')

    def updateFrame(self, newFrame):
        self.last_frame = newFrame

    def killStream(self):
        self.streaming = False

    def bindRoutes(self):        
        @self.webserver.route('/video')
        def video_feed():
            self.streaming = True
            return Response(self.serveVideo(),
                mimetype='multipart/x-mixed-replace; boundary=frame')