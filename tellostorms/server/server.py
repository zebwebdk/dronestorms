import threading
import stormSocket
import videoStream as vs
from lib.tello import Tello
import time

drone = Tello('', 8889)

vidStream = None
useCV = True
codeThread = None

moveToTag = False
moveToTagID = None

class FuncThread(threading.Thread):
    def __init__(self, target, *args):
        self._target = target
        self._args = args
        threading.Thread.__init__(self)

    def run(self):
        self._target(*self._args)

CMD_TIMEOUT = 8

def droneCMD(cmd):
    global fakeResponse
    drone.send_command(cmd)    
    response = None    
    cmdTimeout = time.time() + CMD_TIMEOUT
    while response != 'ok':                    
            response = drone.get_response() or fakeResponse
            if time.time() > cmdTimeout:
                response = 'ok'
    print("CMD Response", response)    
    fakeResponse = None

fakeResponse = None

def setFakeDroneResponse():
    global fakeResponse
    fakeResponse = 'ok'

def executeExternalCode(code):
    # stop cv move to and respond with a "fake ok" to clear all commands"    
    #setFakeDroneResponse()
    exec(code)

def getMoveToTag():
    global moveToTag, moveToTagID10
    return [moveToTag, moveToTagID]

def pingQuad():
    drone.send_command('ping')

def setMoveToTag(shouldMove, tagID):
    global moveToTag, moveToTagID
    moveToTag = shouldMove
    moveToTagID = tagID

def setMoveToAnyTag():
    global detectedTags
    if len(detectedTags) > 0:
        setMoveToTag(True, detectedTags[0].tag_id)

def handlePythonExecution(code):
    global codeThread     
    if fakeResponse == 'ok':           
        setFakeDroneResponse()
        setMoveToTag(False, None)

    if codeThread != None:
        codeThread.join()

    codeThread = FuncThread(executeExternalCode, code)
    codeThread.daemon = True
    codeThread.start()

def handleDroneCommand(cmd):        
    cmdThread = FuncThread(droneCMD, cmd)
    cmdThread.start()
    cmdThread.join()
    return 'DONE'

def shouldUseCV():
    global useCV    
    return useCV

def setUseCV(shouldI):
    global useCV
    print("Should i use CV Python?", shouldI)
    useCV = shouldI

def getDroneState():
    #print("Getting state")
    return {'Battery': drone.get_battery(), 'Flight Time': drone.get_flight_time(), 'Height': drone.get_height(), 'Speed': drone.get_speed()}

def SocketHandler():
    stormSocket.StormSocketServer(handleDroneCommand, getDroneState, setUseCV, handlePythonExecution, setMoveToTag, setFakeDroneResponse)    

def VideoStreamHandler():
    global vidStream
    vidStream = vs.VideoStreamPasser(drone, shouldUseCV, getMoveToTag, setMoveToTag, setFakeDroneResponse, setDetectedTags)

detectedTags = []

def setDetectedTags(tags):
    global detectedTags
    detectedTags = tags

def isTagDetected(tagID):
    global detectedTags
    isDetected = False
    for tag in detectedTags:
        if tag.tag_id == tagID:
            isDetected = True
            break
    return isDetected

def isAnyTagDetected():
    global detectedTags
    if len(detectedTags) > 0:
        return True
    else:
        return False

socketThread = threading.Thread(target = SocketHandler)
socketThread.daemon = True
socketThread.start()

streamThread = threading.Thread(target = VideoStreamHandler)
streamThread.daemon = True
streamThread.start()

while True:
    pass

vidStream.killStream()