from flask import Flask, render_template
from flask_socketio import SocketIO, emit
import json
import time

TAKEOFF_DELAY = 5.5
MOVE_DELAY = 3.5
class StormSocketServer():
    

    def __init__(self, droneCMD, getState, useCV, extExec, setMoveTag, fakeResponse):
        print("Socket Server initated")
        self.droneCMD = droneCMD
        self.extExec = extExec
        self.setMoveTag = setMoveTag
        self.fakeResponse = fakeResponse
        self.useCV = useCV
        self.getState = getState
        self.app = Flask(__name__)
        self.app.config['SECRET_KEY'] = 'secret!'
        self.socketio = SocketIO(self.app)
        self.bindRoutes()
        self.socketio.run(self.app)

    def bindRoutes(self):
        @self.app.route('/')
        def index(self):
            return render_template('index.html')

        @self.socketio.on('connect')
        def handleConnection():
            print("Client Connected")
        
        @self.socketio.on('message')
        def handleMessage(message):            
            #print "MSG", message['cmd'].decode()        
            if 'cmd' in message:
                self.droneCMD(message['cmd'])
            
            if 'useCV' in message:
                self.useCV(message['useCV'])
            
            if 'state' in message:
                state = self.getState()                
                #print("Getting state", state)
                self.socketio.send(state)

            if 'python' in message:
                print("Executing Python", message['python'])
                #self.fakeResponse()
                #self.setMoveTag(False, None)
                self.extExec(message['python'])

            if 'code' in message:
                commands = message['code'].split('\n')
                print("Executing:", commands)
                for cmd in commands:
                    if cmd != '' and cmd != '\n':
                        cmd = cmd.rstrip()                                                                        
                        self.droneCMD(cmd)                            
                        #time.sleep(cmdDelay)

        
    

    


"""

from flask import Flask, render_template
from flask_socketio import SocketIO, emit

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)

@app.route('/')
def index():
    return render_template('index.html')

@socketio.on('my event', namespace='/test')
def test_message(message):
    emit('my response', {'data': message['data']})

@socketio.on('my broadcast event', namespace='/test')
def test_message(message):
    emit('my response', {'data': message['data']}, broadcast=True)

@socketio.on('connect', namespace='/test')
def test_connect():
    emit('my response', {'data': 'Connected'})

@socketio.on('disconnect', namespace='/test')
def test_disconnect():
    print('Client disconnected')

if __name__ == '__main__':
    socketio.run(app)
    """