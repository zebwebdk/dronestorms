/*Blockly.defineBlocksWithJsonArray([{
  "type": "arm_quad",
  "message0": "Arm Quadcopter",
  "nextStatement": null,
  "colour": 0,
  "tooltip": "Send ARM Command to Quadcopter",
  "helpUrl": ""
},
{
  "type": "quad_rise",
  "message0": "Quad Higher %1 Time %2 %3",
  "args0": [
    {
      "type": "input_dummy"
    },
    {
      "type": "field_number",
      "name": "seconds",
      "value": 5,
      "min": 0
    },
    {
      "type": "input_value",
      "name": "NAME"
    }
  ],
  "previousStatement": null,
  "nextStatement": null,
  "colour": 230,
  "tooltip": "",
  "helpUrl": ""
},
{
  "type": "quad_fall",
  "message0": "Quad Lower %1 Time %2 %3",
  "args0": [
    {
      "type": "input_dummy"
    },
    {
      "type": "field_number",
      "name": "seconds",
      "value": 0
    },
    {
      "type": "input_value",
      "name": "NAME"
    }
  ],
  "previousStatement": null,
  "nextStatement": null,
  "colour": 230,
  "tooltip": "Lower the quad altitude",
  "helpUrl": ""
},
{
  "type": "quad_left",
  "message0": "Quad Left %1 Time %2 %3",
  "args0": [
    {
      "type": "input_dummy"
    },
    {
      "type": "field_number",
      "name": "seconds",
      "value": 5,
      "min": 0
    },
    {
      "type": "input_value",
      "name": "NAME",
      "check": "Number"
    }
  ],
  "previousStatement": null,
  "nextStatement": null,
  "colour": 135,
  "tooltip": "Navigate Quadcopter Left",
  "helpUrl": ""
},
{
  "type": "quad_right",
  "message0": "Quad Right %1 Time %2 %3",
  "args0": [
    {
      "type": "input_dummy"
    },
    {
      "type": "field_number",
      "name": "seconds",
      "value": 5,
      "min": 0
    },
    {
      "type": "input_value",
      "name": "NAME",
      "check": "Number"
    }
  ],
  "previousStatement": null,
  "nextStatement": null,
  "colour": 135,
  "tooltip": "Navigate Quadcopter Right",
  "helpUrl": ""
},
{
  "type": "quad_fwd",
  "message0": "Quad Forward %1 Time %2 %3",
  "args0": [
    {
      "type": "input_dummy"
    },
    {
      "type": "field_number",
      "name": "seconds",
      "value": 5,
      "min": 0
    },
    {
      "type": "input_value",
      "name": "NAME"
    }
  ],
  "previousStatement": null,
  "nextStatement": null,
  "colour": 135,
  "tooltip": "",
  "helpUrl": ""
},
{
  "type": "quad_hover",
  "message0": "Quad Hover %1 %2 %3",
  "args0": [
    {
      "type": "input_dummy"
    },
    {
      "type": "field_number",
      "name": "seconds",
      "value": 5,
      "min": 0
    },
    {
      "type": "input_value",
      "name": "NAME"
    }
  ],
  "previousStatement": null,
  "nextStatement": null,
  "colour": 135,
  "tooltip": "",
  "helpUrl": ""
},
{
  "type": "quad_back",
  "message0": "Quad Back %1 Time %2 %3",
  "args0": [
    {
      "type": "input_dummy"
    },
    {
      "type": "field_number",
      "name": "seconds",
      "value": 5,
      "min": 0
    },
    {
      "type": "input_value",
      "name": "NAME"
    }
  ],
  "previousStatement": null,
  "nextStatement": null,
  "colour": 135,
  "tooltip": "",
  "helpUrl": ""
},
{
  "type": "quad_sethead",
  "message0": "Quad Heading %1 %2 %3",
  "args0": [
    {
      "type": "input_dummy"
    },
    {
      "type": "field_number",
      "name": "HEADING",
      "value": 5,
      "min": 0
    },
    {
      "type": "input_value",
      "name": "NAME"
    }
  ],
  "previousStatement": null,
  "nextStatement": null,
  "colour": 135,
  "tooltip": "",
  "helpUrl": ""
},
{
  "type": "quad_disarm",
  "message0": "Disarm Quad",
  "previousStatement": null,
  "colour": 0,
  "tooltip": "Disarm Quad",
  "helpUrl": ""
},
{
  "type": "quad_sensor_altitude",
  "message0": "Sensor: Altitude",
  "output": null,
  "colour": 270,
  "tooltip": "Read Altitude from Quad",
  "helpUrl": ""
},
{
  "type": "quad_attitude",
  "message0": "Quad Attitude",
  "output": null,
  "colour": 270,
  "tooltip": "Read Current Attitude from Quad",
  "helpUrl": ""
},
{
  "type": "quad_doUntil",
  "message0": "for %1 second(s)",
  "args0": [{
    "type": "field_number",
    "name": "SECONDS",
    "value": 10,
    "min": 1,
    "precision": 0.1
  }],
  "message1": "repeat %1",
  "args1": [{
    "type": "input_statement",
    "name": "DO"
  }],
  "previousStatement": null,
  "nextStatement": null,
  "style": "loop_blocks",
  "helpUrl": "",  
}
]);
*/

Blockly.defineBlocksWithJsonArray(
  [{
    "type": "tello_storm_takeoff",
    "message0": "Takeoff",
    "nextStatement": null,
    "colour": 0,
    "tooltip": "Send TAKEOFF Command to Quadcopter",
    "helpUrl": ""
  },
  {
    "type": "tello_storm_land",
    "message0": "Land",
    "previousStatement": null,
    "colour": 0,
    "tooltip": "Send LAND Command to Quadcopter",
    "helpUrl": ""
  },
  {
    "type": "tello_storm_flip",
    "message0": "Flip",
    "nextStatement": null,    
    "previousStatement": null,    
    "colour": 0,
    "tooltip": "Send flip Command to Quadcopter",
    "helpUrl": ""
  },
  {
    "type": "tello_storm_idle",
    "message0": "Quad Idle %1 Time %2 %3",
    "args0": [
      {
        "type": "input_dummy"
      },
      {
        "type": "field_number",
        "name": "seconds",
        "value": 5,
        "min": 0
      },
      {
        "type": "input_value",
        "name": "NAME"
      }
    ],
    "previousStatement": null,
    "nextStatement": null,
    "colour": 230,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "tello_storm_forward",
    "message0": "Quad Forward %1 cm %2 %3",
    "args0": [
      {
        "type": "input_dummy"
      },
      {
        "type": "field_number",
        "name": "distance",
        "value": 20,
        "min": 20
      },
      {
        "type": "input_value",
        "name": "NAME"
      }
    ],
    "previousStatement": null,
    "nextStatement": null,
    "colour": 230,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "tello_storm_cw",
    "message0": "Quad Rotate CW %1 degrees %2 %3",
    "args0": [
      {
        "type": "input_dummy"
      },
      {
        "type": "field_number",
        "name": "degrees",
        "value": 20,        
        "min": 1,
        "max": 3600
      },
      {
        "type": "input_value",
        "name": "NAME"
      }
    ],
    "previousStatement": null,
    "nextStatement": null,
    "colour": 230,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "tello_storm_ccw",
    "message0": "Quad Rotate CCW %1 degrees %2 %3",
    "args0": [
      {
        "type": "input_dummy"
      },
      {
        "type": "field_number",
        "name": "degrees",
        "value": 20,
        "min": 1,
        "max": 3600
      },
      {
        "type": "input_value",
        "name": "NAME"
      }
    ],
    "previousStatement": null,
    "nextStatement": null,
    "colour": 230,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "tello_storm_back",
    "message0": "Quad Back %1 cm %2 %3",
    "args0": [
      {
        "type": "input_dummy"
      },
      {
        "type": "field_number",
        "name": "distance",
        "value": 20,
        "min": 20
      },
      {
        "type": "input_value",
        "name": "NAME"
      }
    ],
    "previousStatement": null,
    "nextStatement": null,
    "colour": 230,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "tello_storm_left",
    "message0": "Quad Left %1 cm %2 %3",
    "args0": [
      {
        "type": "input_dummy"
      },
      {
        "type": "field_number",
        "name": "distance",
        "value": 20,
        "min": 20
      },
      {
        "type": "input_value",
        "name": "NAME"
      }
    ],
    "previousStatement": null,
    "nextStatement": null,
    "colour": 230,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "tello_storm_right",
    "message0": "Quad Right %1 cm %2 %3",
    "args0": [
      {
        "type": "input_dummy"
      },
      {
        "type": "field_number",
        "name": "distance",
        "value": 20,
        "min": 20
      },
      {
        "type": "input_value",
        "name": "NAME"
      }
    ],
    "previousStatement": null,
    "nextStatement": null,
    "colour": 230,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "tello_storm_up",
    "message0": "Quad Up %1 cm %2 %3",
    "args0": [
      {
        "type": "input_dummy"
      },
      {
        "type": "field_number",
        "name": "distance",
        "value": 20,
        "min": 20
      },
      {
        "type": "input_value",
        "name": "NAME"
      }
    ],
    "previousStatement": null,
    "nextStatement": null,
    "colour": 230,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "tello_enable_cv",
    "message0": "Quad Enable Machine Vision",    
    "previousStatement": null,
    "nextStatement": null,
    "colour": 230,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "tello_disable_cv",
    "message0": "Quad Disable Machine Vision",    
    "previousStatement": null,
    "nextStatement": null,
    "colour": 230,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "tello_storm_down",
    "message0": "Quad Down %1 cm %2 %3",
    "args0": [
      {
        "type": "input_dummy"
      },
      {
        "type": "field_number",
        "name": "distance",
        "value": 20,
        "min": 20
      },
      {
        "type": "input_value",
        "name": "NAME"
      }
    ],
    "previousStatement": null,
    "nextStatement": null,
    "colour": 230,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "tellostorm_specific_tag_detected",
    "message0": "Has Quad detected Tag %1 %2",
    "args0": [
      {
        "type": "input_dummy",        
      },
      {
        "type": "input_value",
        "name": "TAG"
      }
    ],
    "output": null,
    "colour": 210,
    "tooltip": "Check if Quad has detected a tag"
  },
  {
    "type": "tellostorm_any_tag_detected",
    "message0": "Has Quad detected any Tag %1",
    "args0": [
      {
        "type": "input_dummy",        
      },      
    ],
    "output": null,
    "colour": 210,
    "tooltip": "Check if Quad has detected any tag"
  },
  {
    "type": "tellostorm_move_to_tag",
    "message0": "Quad Move To Tag %1 %2",
    "args0": [
      {
        "type": "input_dummy",        
      },
      {
        "type": "input_value",
        "name": "TAG"
      }
    ],
    "previousStatement": null,
    "nextStatement": null,
    "colour": 150,
    "tooltip": "Tell the Quad to move to Tag"
  },
  {
    "type": "tellostorm_move_to_any_tag",
    "message0": "Quad Move To Any Tag %1",
    "args0": [
      {
        "type": "input_dummy",        
      }
    ],
    "previousStatement": null,
    "nextStatement": null,
    "colour": 150,
    "tooltip": "Tell the Quad to move to nearest Tag"
  },
  {    
    "type": "tellostorm_tags",
    "message0": "Tag %1",
    "output": null,
    "args0": [
      {
        "type": "field_dropdown",
        "colour": 160,
        "name": "TAG",
        "options": [
          ["none", "NONE"],
          [{"src": "images/tags/tag36h11_0.png", "width": 36, "height": 50, "alt": "Tag 0"}, "0"],
          [{"src": "images/tags/tag36h11_1.png", "width": 36, "height": 50, "alt": "Tag 1"}, "1"],
          [{"src": "images/tags/tag36h11_2.png", "width": 36, "height": 50, "alt": "Tag 2"}, "2"],
          [{"src": "images/tags/tag36h11_3.png", "width": 36, "height": 50, "alt": "Tag 3"}, "3"],
          [{"src": "images/tags/tag36h11_4.png", "width": 36, "height": 50, "alt": "Tag 4"}, "4"],
          [{"src": "images/tags/tag36h11_5.png", "width": 36, "height": 50, "alt": "Tag 5"}, "5"],
          [{"src": "images/tags/tag36h11_6.png", "width": 36, "height": 50, "alt": "Tag 6"}, "6"],
          [{"src": "images/tags/tag36h11_7.png", "width": 36, "height": 50, "alt": "Tag 7"}, "7"],
          [{"src": "images/tags/tag36h11_8.png", "width": 36, "height": 50, "alt": "Tag 8"}, "8"],
          [{"src": "images/tags/tag36h11_9.png", "width": 36, "height": 50, "alt": "Tag 9"}, "9"],
          [{"src": "images/tags/tag36h11_10.png", "width": 36, "height": 50, "alt": "Tag 10"}, "10"],
          [{"src": "images/tags/tag36h11_11.png", "width": 36, "height": 50, "alt": "Tag 11"}, "11"],
          [{"src": "images/tags/tag36h11_12.png", "width": 36, "height": 50, "alt": "Tag 12"}, "12"],
          [{"src": "images/tags/tag36h11_13.png", "width": 36, "height": 50, "alt": "Tag 13"}, "13"],
          [{"src": "images/tags/tag36h11_14.png", "width": 36, "height": 50, "alt": "Tag 14"}, "14"],
          [{"src": "images/tags/tag36h11_15.png", "width": 36, "height": 50, "alt": "Tag 15"}, "15"],
          [{"src": "images/tags/tag36h11_16.png", "width": 36, "height": 50, "alt": "Tag 16"}, "16"],
          [{"src": "images/tags/tag36h11_17.png", "width": 36, "height": 50, "alt": "Tag 17"}, "17"],
          [{"src": "images/tags/tag36h11_18.png", "width": 36, "height": 50, "alt": "Tag 18"}, "18"],
          [{"src": "images/tags/tag36h11_19.png", "width": 36, "height": 50, "alt": "Tag 19"}, "19"],
          [{"src": "images/tags/tag36h11_20.png", "width": 36, "height": 50, "alt": "Tag 20"}, "20"],
          [{"src": "images/tags/tag36h11_21.png", "width": 36, "height": 50, "alt": "Tag 21"}, "21"],
          [{"src": "images/tags/tag36h11_22.png", "width": 36, "height": 50, "alt": "Tag 22"}, "22"],
          [{"src": "images/tags/tag36h11_23.png", "width": 36, "height": 50, "alt": "Tag 23"}, "23"],
          [{"src": "images/tags/tag36h11_24.png", "width": 36, "height": 50, "alt": "Tag 24"}, "24"],          
          [{"src": "images/tags/tag36h11_25.png", "width": 36, "height": 50, "alt": "Tag 25"}, "25"],          
        ]
      }
    ]
  }
  
  ]);