"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var socket_io_client_1 = __importDefault(require("socket.io-client"));
var EventEmitter = require('events');
var SocketCommunicator = /** @class */ (function (_super) {
    __extends(SocketCommunicator, _super);
    function SocketCommunicator() {
        var _this = _super.call(this) || this;
        console.log('SocketCommunication Initiated');
        _this.socket = socket_io_client_1.default('http://localhost:5000');
        _this.handleEvents(_this.socket);
        return _this;
    }
    SocketCommunicator.prototype.handleEvents = function (socket) {
        var _this = this;
        socket.on('connect', function () {
            console.log('Backend Connected');
            _this.emit('backend_connected');
        });
        socket.on('disconnect', function () {
            console.log('Backend Disconnected');
            _this.emit('backend_disconnected');
        });
        socket.on('message', function (msg) {
            _this.emit('backend_message', msg);
        });
    };
    SocketCommunicator.prototype.sendCommand = function (cmd) {
        this.socket.send({
            cmd: cmd
        });
    };
    SocketCommunicator.prototype.setUseCV = function (shouldI) {
        console.log('Should i enable cv?', shouldI);
        var enable = 'False';
        if (shouldI) {
            enable = 'True';
        }
        this.socket.send({
            useCV: enable
        });
    };
    SocketCommunicator.prototype.getState = function () {
        this.socket.send({
            state: 'state'
        });
    };
    SocketCommunicator.prototype.sendEmergency = function () {
        this.socket.send({
            cmd: 'emergency'
        });
    };
    SocketCommunicator.prototype.sendPython = function (python) {
        console.log('Sending Python Code');
        this.socket.send({
            python: python
        });
    };
    SocketCommunicator.prototype.sendCode = function (code) {
        this.socket.send({
            code: code
        });
    };
    return SocketCommunicator;
}(EventEmitter));
exports.SocketCommunicator = SocketCommunicator;
//# sourceMappingURL=socketComms.js.map