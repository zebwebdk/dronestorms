"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Main = /** @class */ (function () {
    function Main() {
    }
    Main.onWindowAllClosed = function () {
        if (process.platform !== 'darwin') {
            Main.application.quit();
        }
    };
    Main.onClose = function () {
        Main.mainWindow = null;
    };
    Main.onReady = function () {
        Main.mainWindow = new Main.BrowserWindow({
            width: 800,
            height: 800,
            webPreferences: {
                nodeIntegration: true
            }
        });
        if (Main.mainWindow !== null) {
            Main.mainWindow.setMenuBarVisibility(false);
            Main.mainWindow.maximize();
            Main.mainWindow.loadURL('file://' + __dirname + '/public/index.html');
            Main.mainWindow.on('closed', Main.onClose);
        }
    };
    Main.main = function (app, browserWindow) {
        Main.BrowserWindow = browserWindow;
        Main.application = app;
        Main.application.on('window-all-closed', Main.onWindowAllClosed);
        Main.application.on('ready', Main.onReady);
    };
    return Main;
}());
exports.default = Main;
exports.TestFunc = function (str) {
    console.log('Someone clicked something!', str);
};
//# sourceMappingURL=main.js.map