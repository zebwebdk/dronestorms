"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var socket_io_client_1 = __importDefault(require("socket.io-client"));
var socket = socket_io_client_1.default('http://localhost:5000');
socket.on('connect', function () {
    console.log('connected');
    socket.send({
        cmd: 'takeoff'
    });
});
socket.on('event', function (data) {
    console.log('data', data);
});
socket.on('disconnect', function () {
    console.log('disconnected');
});
socket.on('message', function (msg) {
    console.log('Message', msg);
});
//# sourceMappingURL=socket-test.js.map