import * as wifi from 'node-wifi';

export const wifiManager = async () => {
    try {        
        wifi.init({
            iface: 'wlp3s0'
        })

        wifi.scan((err, networks) => {
            if(err) return console.error(err);
            console.log('Networks', networks);
        })
        //console.log('Get wifi info...', wifi.getCurrentConnections());
    } catch (e) {
        console.error(e)
    }
}

wifiManager();