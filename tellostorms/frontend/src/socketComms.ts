import io from 'socket.io-client';
const EventEmitter = require('events');     

export class SocketCommunicator extends EventEmitter {

    socket;

    constructor() {
        super();
        console.log('SocketCommunication Initiated')
        this.socket = io('http://localhost:5000')
        this.handleEvents(this.socket);
    }

    handleEvents(socket) {
        socket.on('connect', () => {
            console.log('Backend Connected');
            this.emit('backend_connected');
        });

        socket.on('disconnect', () => {
            console.log('Backend Disconnected');
            this.emit('backend_disconnected');
        });

        socket.on('message', (msg) => {
            this.emit('backend_message', msg);
        });
    }

    sendCommand(cmd: string) {
        this.socket.send({
            cmd
        });
    }

    setUseCV(shouldI: boolean) {
        console.log('Should i enable cv?', shouldI)
        let enable = 'False'
        if(shouldI) {
            enable = 'True'
        }
        this.socket.send({
            useCV: enable
        })
    }

    getState() {
        this.socket.send({
            state: 'state'
        })
    }

    sendEmergency() {
        this.socket.send({
            cmd: 'emergency'
        })
    }

    sendPython(python: string) {
        console.log('Sending Python Code')
        this.socket.send({
            python
        })
    }

    sendCode(code: string) {
        this.socket.send({
            code
        })
    }

}