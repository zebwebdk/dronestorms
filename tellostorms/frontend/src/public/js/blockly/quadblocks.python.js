Blockly.Python['tello_storm_takeoff'] = function(block) {
  return 'droneCMD("takeoff")\n'
}

Blockly.Python['tello_storm_land'] = function(block) {
  return 'droneCMD("land")\n'
}

Blockly.Python['tello_storm_idle'] = function(block) {
  var number_seconds = block.getFieldValue('seconds');
  return 'time.sleep(' + number_seconds + ')\n'
}

Blockly.Python['tello_storm_forward'] = function(block) {
  var distance = block.getFieldValue('distance');
  return 'droneCMD("forward ' + distance + '")\n';
}

Blockly.Python['tello_storm_flip'] = function(block) {  
  return 'droneCMD("flip b")\n';
}

Blockly.Python['tello_storm_cw'] = function(block) {
  var degrees = block.getFieldValue('degrees');
  return 'droneCMD("cw ' + degrees + '")\n';
}

Blockly.Python['tello_storm_ccw'] = function(block) {
  var degrees = block.getFieldValue('degrees');
  return 'droneCMD("ccw ' + degrees + '")\n';
}



Blockly.Python['tello_storm_back'] = function(block) {
  var distance = block.getFieldValue('distance');
  return 'droneCMD("back ' + distance + '")\n';
}

Blockly.Python['tello_storm_left'] = function(block) {
  var distance = block.getFieldValue('distance');
  return 'droneCMD("left ' + distance + '")\n';
}

Blockly.Python['tello_storm_right'] = function(block) {
  var distance = block.getFieldValue('distance');
  return 'droneCMD("right ' + distance + '")\n';
}

Blockly.Python['tello_storm_up'] = function(block) {
  var distance = block.getFieldValue('distance');
  return 'droneCMD("up ' + distance + '")\n';
}

Blockly.Python['tello_storm_down'] = function(block) {
  var distance = block.getFieldValue('distance');
  return 'droneCMD("down ' + distance + '")\n';
}

Blockly.Python['tellostorm_specific_tag_detected'] = function(block) {  
  const tagID = Blockly.Python.valueToCode(block, 'TAG', Blockly.Python.ORDER_NONE) || 'False'    
  return ['isTagDetected(' + tagID + ')', Blockly.Python.ORDER_ATOMIC];
}

Blockly.Python['tellostorm_any_tag_detected'] = function(block) {  
  const tagID = Blockly.Python.valueToCode(block, 'TAG', Blockly.Python.ORDER_NONE) || 'False'    
  return ['isAnyTagDetected()', Blockly.Python.ORDER_ATOMIC];
}

Blockly.Python['tello_enable_cv'] = function(block) {  
  return 'setUseCV(True)\n';
}

Blockly.Python['tello_disable_cv'] = function(block) {  
  return 'setUseCV(False)\n';
}

Blockly.Python['tellostorm_tags'] = function(block) {
  var tagID = block.getFieldValue('TAG');
  return [tagID, Blockly.Python.ORDER_ATOMIC];
}

Blockly.Python['tellostorm_move_to_tag'] = function(block) {  
  const tagID = Blockly.Python.valueToCode(block, 'TAG', Blockly.Python.ORDER_NONE) || 'False'    
  return 'setMoveToTag(True, ' + tagID + ')\n';
}

Blockly.Python['tellostorm_move_to_any_tag'] = function(block) {  
  const tagID = Blockly.Python.valueToCode(block, 'TAG', Blockly.Python.ORDER_NONE) || 'False'    
  return 'setMoveToAnyTag()\n';
}

Blockly.Python['arm_quad'] = function(block) {
  // TODO: Assemble Python into code variable.
  var code = `navigator.armQuad()\n`;
  return code;
};

Blockly.Python['quad_rise'] = function(block) {
  var number_seconds = block.getFieldValue('seconds');
  var value_name = Blockly.Python.valueToCode(block, 'NAME', Blockly.Python.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  // var code = `print("Quad is rising for", ${number_seconds}, "seconds")\n`;
  code = 'navigator.raiseQuad(' + number_seconds + ')\n';
  return code;
};

Blockly.Python['quad_fall'] = function(block) {
  var number_seconds = block.getFieldValue('seconds');
  var value_name = Blockly.Python.valueToCode(block, 'NAME', Blockly.Python.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  // var code = `print("Quad is falling for", ${number_seconds}, "seconds")\n`;
  code = 'navigator.lowerQuad(' + number_seconds + ')\n';
  return code;
};

Blockly.Python['quad_left'] = function(block) {
  var number_seconds = block.getFieldValue('seconds');
  var value_name = Blockly.Python.valueToCode(block, 'NAME', Blockly.Python.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  var code = `print("Quad is navigating left for", ${number_seconds}, "seconds")\n`;
  return code;
};

Blockly.Python['quad_right'] = function(block) {
  var number_seconds = block.getFieldValue('seconds');
  var value_name = Blockly.Python.valueToCode(block, 'NAME', Blockly.Python.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  var code = `print("Quad is navigating right for", ${number_seconds}, "seconds")\n`;
  return code;
};

Blockly.Python['quad_fwd'] = function(block) {
  var number_seconds = block.getFieldValue('seconds');
  var value_name = Blockly.Python.valueToCode(block, 'NAME', Blockly.Python.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  var code = `print("Quad is navigating forward for", ${number_seconds}, "seconds")\n`;
  return code;
};

Blockly.Python['quad_back'] = function(block) {
  var number_seconds = block.getFieldValue('seconds');
  var value_name = Blockly.Python.valueToCode(block, 'NAME', Blockly.Python.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  var code = `print("Quad is navigating backwards for", ${number_seconds}, "seconds")\n`;
  return code;
};

Blockly.Python['quad_disarm'] = function(block) {
  // TODO: Assemble Python into code variable.
  var code = `navigator.disarmQuad()\n`;
  return code;
};

Blockly.Python['quad_hover'] = function(block) {
  var number_seconds = block.getFieldValue('seconds');
  var value_name = Blockly.Python.valueToCode(block, 'NAME', Blockly.Python.ORDER_ATOMIC);
  // TODO: Assemble Python into code variable.
  // var code = `print("Quad is falling for", ${number_seconds}, "seconds")\n`;
  code = 'hoverQuad(' + number_seconds + ')\n';
  return code;
};

Blockly.Python['quad_sensor_altitude'] = function(block) {  
  return 'getQuadAltitude()\n';
};

Blockly.Python['quad_attitude'] = function(block) {
  return 'getQuadAttitude()\n';
}

Blockly.Python['quad_doUntil'] = function(block) {  
  var seconds = block.getFieldValue('SECONDS');
  var secToMillis = parseFloat(seconds) * 1000;
  console.log(seconds, secToMillis);
  var loopCode = Blockly.Python.statementToCode(block, 'DO');
  console.log('LoopCode', loopCode);
  loopCode = Blockly.Python.addLoopTrap(loopCode, block.id) || Blockly.Python.PASS;
  var code = `startTime = millis() + ${secToMillis}:\n${loopCode}`;
  return code;
}

Blockly.Python['quad_sethead'] = function(block) {
  var heading = block.getFieldValue('HEADING');
  return 'setHead(' + heading + ')\n';
}
