const electron = require('electron');
const backendComms = electron.remote.require('./app').socketComms;
const fs = require('fs');
const { dialog } = require('electron').remote;
require('./js/smalltalk.native.min.js');

const Blockly = require('node-blockly/browser')
require('./js/blockly/blocks/quadblocks.js')
require('./js/blockly/quadblocks.python.js')

/*
<script src="js/blockly/blocks_compressed.js"></script>
<script src="js/blockly/blocks/quadblocks.js"></script>
<script src="js/blockly/python_compressed.js"></script>
<script src="js/blockly/quadblocks.python.js"></script>
<script src="js/blockly/msg/js/en.js"></script>
*/


Blockly.prompt = (a, b, c) => {    
    console.log('a',a ,'b', b,'c', c)
}

const stateInterval = setInterval(() => {    
    backendComms.getState()
}, 1000)

const runBtn = document.querySelector('.runBtn');
runBtn.addEventListener('click', () => {
    const code = Blockly.Python.workspaceToCode(workspace);
    console.log('run btn pressed', code);
    backendComms.sendPython(code);

});

const saveBtn = document.querySelector('.saveBtn');
saveBtn.addEventListener('click', () => {
    const savePath = dialog.showSaveDialog();    
    if(savePath) {
        console.log('saving workspace to', savePath);
        const workspaceToSave = Blockly.Xml.workspaceToDom( Blockly.mainWorkspace );
        const s = new XMLSerializer()
        const serializedWorkspace = s.serializeToString(workspaceToSave);
        fs.writeFileSync(savePath, serializedWorkspace);
        alert("Workspace Saved")
    }
})

let shouldUseCV = false;
const cvBtn = document.querySelector('.cvBtn');
cvBtn.addEventListener('click', () => {
    shouldUseCV = !shouldUseCV;
    backendComms.setUseCV(shouldUseCV);
});

const loadBtn = document.querySelector('.loadBtn');
loadBtn.addEventListener('click', () => {
    const loadPath = dialog.showOpenDialog();
    console.log('loadpath', loadPath[0]);
    if(loadPath) {
        //Blockly.mainWorkspace.clear()
        const restoredWorkspace = fs.readFileSync(loadPath[0]);
        console.log('Loaded Workspace', restoredWorkspace);                
        const xml = Blockly.Xml.textToDom(restoredWorkspace);
        Blockly.Xml.clearWorkspaceAndLoadFromXml(xml, Blockly.mainWorkspace);
        alert("Workspace Loaded");
    }
})

document.addEventListener('keydown', event => {
    if (event.key === 'Escape' || event.keyCode === 27) {        
        backendComms.sendEmergency()
        alert("EMERGENCY STOP!")
    }
});


const abortBtn = document.querySelector('.abortBtn');
abortBtn.addEventListener('click', () => {
    backendComms.sendEmergency()
    alert("EMERGENCY STOP!")
});

const actionButtons = {
    takeoff: document.querySelector('.takeoffBtn'),
    land: document.querySelector('.landBtn'),    
}

actionButtons.takeoff.addEventListener('click', () => {
    backendComms.sendCommand('takeoff');
})

actionButtons.land.addEventListener('click', () => {
    backendComms.sendCommand('land');
})

backendComms.on('backend_connected', () => {
    alert('Backend connected');
});

backendComms.on('backend_disconnect', () => {
    alert('Backend disconnected');
});

backendComms.on('backend_message', (msg) => {
    // this is a state message
    if(msg.Battery) {
        // set battery state 
        const batElm = document.querySelector('#batteryIndicator');
        if(parseInt(msg.Battery) > 70) {
            batElm.className = 'icon icon-progress-3';
            batElm.style.cssText = 'color: green;';
        } else if (parseInt(msg.Battery) > 35) {
            batElm.className = 'icon icon-progress-2';
            batElm.style.cssText = 'color: black;';
        } else if (parseInt(msg.Battery) > 15) {
            batElm.className = 'icon icon-progress-1';
            batElm.style.cssText = 'color: yellow;';
        } else {
            batElm.className = 'icon icon-progress-0';
            batElm.style.cssText = 'color: red;';
        }
    }

    if(msg['Flight Time']) {
        document.querySelector('#currentFlightTime').innerHTML = msg['Flight Time'];
    }

    if(msg.Height) {
        document.querySelector('#currentHeight').innerHTML = parseInt(msg.Height) * 10;
    }

    if(msg.Speed) {
        //document.querySelector('#currentSpeed').innerHTML = msg.Speed;
    }
    console.log('Message', msg);
})

/* BLOCKLY STUFF */

var toolbox = document.getElementById("toolbox");

var options = { 
    toolbox : toolbox, 
    autoClose: false,
    collapse : false, 
    comments : false, 
    disable : false, 
    maxBlocks : Infinity, 
    trashcan : false, 
    horizontalLayout : false, 
    toolboxPosition : 'start', 
    css : true, 
    media : 'https://blockly-demo.appspot.com/static/media/', 
    rtl : false, 
    scrollbars : false, 
    sounds : true, 
    oneBasedIndex : true
};

var workspace = Blockly.inject('blocklyDiv', options);

var workspaceBlocks = document.getElementById("workspaceBlocks"); 

Blockly.Xml.domToWorkspace(workspaceBlocks, workspace);

const updateCodePreview = () => {
    const code = Blockly.Python.workspaceToCode(workspace);
    console.log('Code', code);            
    const previewElement = document.getElementById('codePreview');
    const previewCode = '\n' + code + '\n';
    previewElement.innerHTML = previewCode;            
    Prism.highlightElement(previewElement);
};

workspace.addChangeListener(updateCodePreview);