import NodeTello from './tello-api';
import { lookup } from 'dns';

const drone = new NodeTello()

const loop = async () => {
    drone.init()
    await new Promise(done => setTimeout(done, 10000));
    drone.tello_cmd({
        cmd: {
            cmd: 'takeoff'
        }
    })
    await new Promise(done => setTimeout(done, 5000));
    drone.tello_cmd({
        cmd: {
            cmd: 'land'
        }
    })
    await new Promise(done => setTimeout(done, 5000));
}



loop()