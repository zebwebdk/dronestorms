import { app, BrowserWindow } from 'electron';
import Main from './main';

import { SocketCommunicator } from './socketComms';

export const socketComms = new SocketCommunicator();

// launch electron app
Main.main(app, BrowserWindow);
