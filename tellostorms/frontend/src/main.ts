import { BrowserWindow } from 'electron';

export default class Main {
    static mainWindow: Electron.BrowserWindow | null;
    static application: Electron.App;
    static BrowserWindow;

    private static onWindowAllClosed() {
        if (process.platform !== 'darwin') {
            Main.application.quit();
        }
    }

    private static onClose() {
        Main.mainWindow = null;
    }

    private static onReady() {
        Main.mainWindow = new Main.BrowserWindow({
            width: 800,
            height: 800,
            webPreferences: {
                nodeIntegration: true
            }
        });
        if (Main.mainWindow !== null) {
            Main.mainWindow.setMenuBarVisibility(false)
            Main.mainWindow.maximize();
            Main.mainWindow.loadURL('file://' + __dirname + '/public/index.html');
            Main.mainWindow.on('closed', Main.onClose);
        }
    }

    static main(app: Electron.App, browserWindow: typeof BrowserWindow) {
        Main.BrowserWindow = browserWindow;
        Main.application = app;
        Main.application.on('window-all-closed', Main.onWindowAllClosed);
        Main.application.on('ready', Main.onReady);
    }    
}

export const TestFunc = (str: string) => {
    console.log('Someone clicked something!', str);
}