import io from 'socket.io-client';
const socket = io('http://localhost:5000')

socket.on('connect', function(){
    console.log('connected')    
    socket.send({
        cmd: 'takeoff'
    })
});
socket.on('event', function(data){
    console.log('data', data)
});
socket.on('disconnect', function(){
    console.log('disconnected')
});
socket.on('message', (msg) => {
    console.log('Message', msg)
})