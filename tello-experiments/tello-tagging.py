from lib.tello import Tello
import cv2
import time
import numpy as np
import apriltag

drone = Tello('', 8889)
detector = apriltag.Detector()

drone.takeoff()
CORRECTION_SLEEP = 0.5
last_correction = None
current_milli_time = lambda: int(round(time.time() * 1000))

while True:
    frame = drone.read()        
    if frame is None or frame.size == 0:
        print("No frame")
        continue 

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    colorized = cv2.cvtColor(gray, cv2.COLOR_GRAY2RGB)
    result = detector.detect(gray)

    targetX = colorized.shape[0] / 2
    targetY = colorized.shape[1] / 2    

    for tag in result:
        #print("Tag ID:", tag.tag_id, tag.center)
        cv2.rectangle(colorized, (int(tag.corners[0][0]), int(tag.corners[0][1])), (int(tag.corners[2][0]), int(tag.corners[2][1])), (0, 255, 0), 2)
        cv2.putText(colorized, str(tag.tag_id), (int(tag.center[0]), int(tag.center[1])), cv2.FONT_HERSHEY_SIMPLEX, 4, (0, 255, 0), 2, cv2.LINE_AA)
        if tag.tag_id == 24:
            x, y = 0, 0
            print("Moving to tag",targetX, targetY, tag.center[0], tag.center[1])
            if int(tag.center[0]) > targetX:
                x = 20
            else:
                x = -20

            if int(tag.center[1]) > targetY:
                y = 20
            else:
                y = -20

            drone.send_command('go ' + str(x*-1) + ' ' + str(y*-1) + ' 0 50')
            time.sleep(CORRECTION_SLEEP)
            

    # PRINT IMAGE
    cv2.namedWindow('image', cv2.WINDOW_NORMAL)
    #print("CONV", frame)
    cv2.imshow('image', colorized)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break


drone.land()
cv2.destroyAllWindows()