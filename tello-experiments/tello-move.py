from lib.tello-cmd import Tello

commands = ['takeoff', 'down 400', 'land']

tello = Tello()

for cmd in commands:
    if cmd != '' and cmd != '\n':
        cmd = cmd.rstrip()

        tello.send_command(cmd)

print("Done!")