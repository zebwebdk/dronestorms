import cv2
import apriltag
from math import sqrt
import numpy as np
from lib.tello import Tello
import time

drone = Tello('', 8889)

def distanceBetweenPoints(p1, p2):    
    return sqrt(((p1[0]-p2[0])**2)+((p1[1]-p2[1])**2))

def pixel2mmRatio(pixels, mm):
    return mm / pixels

#cap = cv2.VideoCapture(0)
detector = apriltag.Detector()

CORRECTION_TIMEOUT = 0.1

drone.takeoff()
drone.send_command('speed 10')
while True:

    frame = drone.read()        
    if frame is None or frame.size == 0:
        print("No frame")
        continue 
 
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    colorImage = cv2.cvtColor(gray, cv2.COLOR_GRAY2RGB)
    tags = detector.detect(gray)

    
    #centerX = int(colorImage.shape[0] / 2)
    #centerY = int(colorImage.shape[1] / 2)
    centerX = 460
    centerY = 360

    if len(tags) == 0:
        drone.send_command("rc 0 0 0 0")

    for tag in tags:
        #cv2.rectangle(colorImage, (int(tag.corners[0][0]), int(tag.corners[0][1])), (int(tag.corners[2][0]), int(tag.corners[2][1])), (0, 255, 0), 2)                        
        pts = np.array([tag.corners[0], tag.corners[1], tag.corners[2], tag.corners[3]], np.int32)
        cv2.polylines(colorImage,[pts],True,(0,255,255))
        dist = distanceBetweenPoints(tag.corners[0], tag.corners[1])
        pixRatio = pixel2mmRatio(dist, 80)
        distanceToCenter = distanceBetweenPoints(tag.center, (centerX, centerY)) * pixRatio     
        cv2.line(colorImage, (centerX, centerY), (int(tag.center[0]), int(tag.center[1])), (0, 255, 0), 2)
        #print("Tag ID:", tag.tag_id, "80mm in pixels:", dist, "Ratio", pixRatio, "Distance to center", distanceToCenter)
        if tag.tag_id == 22:
            xMove = int(((tag.center[0]- centerX) * pixRatio) / 10)
            yMove = int(((centerY - tag.center[1]) * pixRatio) / 10)
            moveCMD = "rc " + str(xMove) + " " + str(yMove) + " 0 0"
            #print("Quad Move CMD:", moveCMD)
            #drone.send_command('go ' + str(xMove) + ' ' + str(yMove) + ' 0 50')
            drone.send_command(moveCMD)
            
            time.sleep(CORRECTION_TIMEOUT)
        else:
            drone.send_command("rc 0 0 0 0")    

    cv2.imshow('feed', colorImage)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

drone.land()
cv2.destroyAllWindows()