import apriltag
import cv2
from djitellopy import Tello

detector = apriltag.Detector()

#result = detector.detect(img)
#cv2.imshow('img', image)
#cv2.waitKey(0)

TAG_ID = 25

tello = Tello()

if not tello.connect():
    print("Tello not connected")    
# In case streaming is on. This happens when we quit this program without the escape key.
if not tello.streamoff():
    print("Could not stop video stream")    

if not tello.streamon():
    print("Could not start video stream")    

if not tello.set_speed(10):
    print("Not set speed to lowest possible")    

cap = tello.get_video_capture()

tello.takeoff()

while True:
    success, frame = cap.read()
    cv2.namedWindow('feed', cv2.WINDOW_NORMAL)
        
    # Our operations on the frame come here
    image = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    colorImg = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)
    

    # April Tagging
    result = detector.detect(image)    
    print("Res", result)
    for tag in result:
        print(tag.center[0], tag.center[1])
        cv2.rectangle(colorImg, (int(tag.corners[0][0]), int(tag.corners[0][1])), (int(tag.corners[2][0]), int(tag.corners[2][1])), (0, 255, 0), 2)
        if tag.tag_id == TAG_ID:
            print("Move to tag")


    # Display the resulting frame
    cv2.imshow('feed',colorImg)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

tello.land()
cap.release()
cv2.destroyAllWindows()
