from djitellopy import Tello
import apriltag
import cv2

detector = apriltag.Detector()
tello = Tello()

if not tello.connect():
    print("Tello not connected")    

if not tello.set_speed(10):
    print("Not set speed to lowest possible")    

# In case streaming is on. This happens when we quit this program without the escape key.
if not tello.streamoff():
    print("Could not stop video stream")    

if not tello.streamon():
    print("Could not start video stream")    

tello.connect()
#cap = tello.get_video_capture()
tello.takeoff()
while True:
    #success, frame = cap.read()
    cv2.namedWindow('feed', cv2.WINDOW_NORMAL)
        
    # Our operations on the frame come here
    image = cv2.cvtColor(cv2.flip(tello.get_frame_read().frame, 0), cv2.COLOR_BGR2GRAY)
    colorImg = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)
    

    # April Tagging
    result = detector.detect(image)    
    #print("Res", result)

    # center coords of the image
    #print("Image Shape:", image.shape)
    targetX = image.shape[1] / 2
    targetY = image.shape[0] / 2
    for tag in result:
        #print(tag.center[0], tag.center[1])
        cv2.rectangle(colorImg, (int(tag.corners[0][0]), int(tag.corners[0][1])), (int(tag.corners[2][0]), int(tag.corners[2][1])), (0, 255, 0), 2)
        if tag.tag_id == 25:
            print("Move to tag")            
            xMove = (targetX - int(tag.center[0])) / 10
            yMove = (targetY - int(tag.center[1])) / 10
            print("Target Coords", targetX, targetY, xMove, yMove)
            
            tello.send_rc_control(int(yMove), int(xMove), 0, 0)
        else:
            tello.send_rc_control(0, 0, 0, 0)

    # Display the resulting frame
    cv2.imshow('feed',colorImg)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

tello.land()
cap.release()
cv2.destroyAllWindows()
tello.end()