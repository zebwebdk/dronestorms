from djitellopy import Tello
import cv2
import numpy as np


tello = Tello()

if not tello.connect():
    print("Tello not connected")    

if not tello.set_speed(10):
    print("Not set speed to lowest possible")    

# In case streaming is on. This happens when we quit this program without the escape key.
if not tello.streamoff():
    print("Could not stop video stream")    

if not tello.streamon():
    print("Could not start video stream")    

#tello.connect()
cap = tello.get_video_capture()
#cap = cv2.VideoCapture(0)
while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Our operations on the frame come here
    gray = cv2.cvtColor(cv2.flip(frame, 0), cv2.COLOR_BGR2GRAY)

    # Display the resulting frame
    cv2.imshow('frame',gray)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
tello.end()
