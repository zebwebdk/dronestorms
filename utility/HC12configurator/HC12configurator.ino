//HC-12 AT Commander
//The HC-12 default baud rate is 9600

/*
while energized, pull Pin 5 (“SET”) low,
wait 40ms for command mode to engage

CHEATSHEET:

  Read single values:
AT+RB - Baud (1200 - 115200)
AT+RC - Channel (001-127)
AT+RF - FU mode (Serial port transparent transmission mode 1/2/3)
AT+RP - TX power (1-8)

  Dump setup:
AT+RX 
  Change baud rate:
AT+B115200
  Change channel:
AT+C001
AT+C127
etc..


*/

const int SETpin = 3; //command pin
bool commandMode = false;
void setup() {
	while(!Serial); //wait for serial connection from PC
  Serial.begin(115200);
  //Serial1.begin(2400);
  Serial1.begin(9600); //DEFAULT VALUE
  //Serial1.begin(115200);

  pinMode(SETpin, OUTPUT);
  digitalWrite(SETpin, !commandMode);

  Serial.println("Now in RF mode. Switch modes with: \\");
  //Serial.println("Now in RF mode. MODE SWITCHING DISABLED.");



}

char rx;

void loop(){
  if (Serial1.available()) Serial.write(Serial1.read()); //dump data directly from HC-12 to PC
  
  //if (Serial.available()) Serial1.write(Serial.read()); //dump data directly from PC to HC12


  if (Serial.available()){
  	rx=Serial.read();

  	if(rx=='\\') {// \\ means '\'', since \ is the escape char
  		commandMode=!commandMode; //flip the bool
  		digitalWrite(SETpin,!commandMode); //set the pin (command mode is active low)
  		if(commandMode) Serial.println("Command mode set.");
  		else Serial.println("RF mode set.");
  		delay(50);
  	}
    
   else Serial1.write(rx);
  }

}
