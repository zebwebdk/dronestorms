/*
 * Decide on rangefinder type here. Only one type should be uncommented
 */


/*
 * Set I2C Slave address
 */
#define I2C_SLAVE_ADDRESS 0x14

// #define DEBUG

#define STATUS_OK 0
#define STATUS_OUT_OF_RANGE 1

#include <Wire.h>

#include <SoftwareSerial.h>
#include "TFMini.h"

// Setup software serial port 
SoftwareSerial mySerial(10, 11);      // Uno RX (TFMINI TX), Uno TX (TFMINI RX)
TFMini tfmini;

#ifndef TWI_RX_BUFFER_SIZE
#define TWI_RX_BUFFER_SIZE ( 16 )
#endif

/*
 * Configuration magic, do not touch
 */
#define MAX_RANGE 1000 //Range of (AT LEAST) 10 meters


uint8_t i2c_regs[] =
{
    0, //status
    0, //older 8 of distance
    0, //younger 8 of distance
};

const byte reg_size = sizeof(i2c_regs);
volatile byte reg_position = 0;

void requestEvent()
{  
    Wire.write(i2c_regs, 3);
}

void receiveEvent(uint8_t howMany) {

    if (howMany < 1) {
        // Sanity-check
        return;
    }

    if (howMany > TWI_RX_BUFFER_SIZE)
    {
        // Also insane number
        return;
    }

    reg_position = Wire.read();

    howMany--;

    if (!howMany) {
        // This write was only to set the buffer for next read
        return;
    }

    // Everything above 1 byte is something we do not care, so just get it from bus as send to /dev/null
    while(howMany--) {
      Wire.read();
    }
}

void setup() {
    /*
     * Setup I2C
     */
    Wire.begin(I2C_SLAVE_ADDRESS);
    Wire.onRequest(requestEvent);
    Wire.onReceive(receiveEvent);

  Serial.begin(115200);

  mySerial.begin(TFMINI_BAUDRATE);
  // Step 3: Initialize the TF Mini sensor
  tfmini.begin(&mySerial);    

}


void loop() {

    static uint32_t nextUpdate = 0;

  /*
   * Measurement is done every 6th wakeup, that gives more less 10Hz update rate (96ms)
   */
  if (millis() > nextUpdate) {
    
    uint16_t strength = tfmini.getRecentSignalStrength();

   
    if (strength > 50 && strength < 65535) {
      i2c_regs[0] = STATUS_OK;
    } else {
      i2c_regs[0] = STATUS_OUT_OF_RANGE;
    }

    uint16_t cm = tfmini.getDistance();
    
    i2c_regs[1] = cm >> 8;
    i2c_regs[2] = cm & 0xFF;

    nextUpdate = millis() + 200;
  }
  
}
