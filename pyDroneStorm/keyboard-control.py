#!/usr/bin/env python

"""keyboard-control.py - keyboard control of quad via MSP"""

__author__ = "Sebastian Hines"
__copyright__ = "Copyright 2019 zebweb.dk"

__license__ = "GPL"
__version__ = "1"
__maintainer__ = "Sebastian Hines"
__email__ = "sebastian@zebweb.dk"
__status__ = "Development"

import time, pygame, sys
from pymultiwii import MultiWii
pygame.init()
pygame.display.set_mode((100, 100))

valIncrement = 5
looptime = 0.1

a = 1500
e = 1500
t = 1000
r = 1500
aux1 = 1000


def pitchUp():    
    global e
    e -= valIncrement

def pitchDown():
    global e
    e += valIncrement

def rollLeft():
    global a
    a -= valIncrement

def rollRight():
    global a
    a += valIncrement

def yawLeft():
    global r
    r -= valIncrement

def yawRight():
    global r
    r += valIncrement

def throttleUp():
    global t
    t += valIncrement

def throttleDown():
    global t
    t -= valIncrement

def levelQuad():
    global a, e, r
    a = 1500
    e = 1500
    r = 1500

def killThrottle():
    t = 1000

def arm():
    global aux1
    aux1 = 2000

def disarm():
    global aux1
    aux1 = 1000

def controlScheme():
    global pygame
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_w:
                pitchUp()
            if event.key == pygame.K_s:
                pitchDown()
            if event.key == pygame.K_a:
                rollLeft()
            if event.key == pygame.K_d:
                rollRight()
            if event.key == pygame.K_q:
                yawLeft()
            if event.key == pygame.K_e:
                yawRight()
            if event.key == pygame.K_PAGEUP:
                throttleUp()
            if event.key == pygame.K_PAGEDOWN:
                throttleDown()
            if event.key == pygame.K_o:
                arm()
            if event.key == pygame.K_l:
                disarm()
            if event.key == pygame.K_SPACE:
                levelQuad()
            if event.key == pygame.K_DELETE:
                killThrottle()
            if event.key == pygame.K_ESCAPE:
                sys.exit()

if __name__ == "__main__":
    # AETR1234
    board = MultiWii("COM3")
    print "connected to board"    
    try:
        board.resetRC()                

        while True:
            controlScheme()                        
            data = [a, e, t, r, aux1]
            board.sendCMD(10, MultiWii.SET_RAW_RC, data)
            print "DATA: " + str(data)
            time.sleep(looptime)

    except Exception,error:
        print ("Error on Main: "+str(error))

"""
board.resetRC()
arm()

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_w:
                pitchUp()
            if event.key == pygame.K_s:
                pitchDown()
            if event.key == pygame.K_a:
                rollLeft()
            if event.key == pygame.K_d:
                rollRight()
            if event.key == pygame.K_q:
                yawLeft()
            if event.key == pygame.K_e:
                yawRight()
            if event.key == pygame.K_PAGEUP:
                throttleUp()
            if event.key == pygame.K_PAGEDOWN:
                throttleDown()
            if event.key == pygame.K_o:
                arm()
            if event.key == pygame.K_l:
                disarm()
            if event.key == pygame.K_ESCAPE:
                sys.exit()

    controldata = [a, e, t, r, aux1]
    print ("Sending ctrl data: " + str(controldata))
    #board.sendCMD(10,MultiWii.SET_RAW_RC,controldata)    
    time.sleep(looptime)
"""