import * as net from 'net';

const server = net.createServer((socket) => {
    
    console.log('Client connected');
    socket.write('Hello client - i am server!\r\n');    

    socket.on('error', (err) => {                
        if(err.message === 'read ECONNRESET') {
            console.error('Client Disconnected --> Connection reset');
        } else {
            console.error(err);
        }
    })

    socket.on('data', (data) => {
        console.log(data.toString('utf-8'));
    })

    setInterval(() => {
        socket.write('sup son?');
    }, 2000);
});

server.listen(1337, '0.0.0.0');
