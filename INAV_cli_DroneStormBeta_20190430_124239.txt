diff

# version
# INAV/KAKUTEF4V2 2.2.0 Apr 16 2019 / 13:39:10 (6544d509e)
# GCC-7.3.1 20180622 (release) [ARM/embedded-7-branch revision 261907]

# resources

# mixer
mmix 0  1.000 -1.000  1.000 -1.000
mmix 1  1.000 -1.000 -1.000  1.000
mmix 2  1.000  1.000  1.000  1.000
mmix 3  1.000  1.000 -1.000 -1.000

# servo mix

# servo

# logic

# feature
feature -TX_PROF_SEL
feature -TELEMETRY
feature -OSD
feature VBAT
feature GPS
feature PWM_OUTPUT_ENABLE

# beeper

# map

# serial
serial 0 0 115200 115200 0 115200
serial 2 1 115200 115200 0 115200
serial 4 2 115200 115200 0 115200
serial 5 65536 115200 115200 0 115200

# led

# color

# mode_color

# aux
aux 0 0 0 1700 2100
aux 1 1 0 900 2100
aux 2 3 0 900 2100
aux 3 9 0 900 2100

# adjrange

# rxrange

# temp_sensor

# wp
#wp 0 invalid

# osd_layout

# master
set looptime = 500
set gyro_hardware_lpf = 256HZ
set gyro_lpf_hz = 150
set gyro_notch1_hz = 400
set gyro_notch1_cutoff = 300
set gyro_notch2_hz = 200
set gyro_notch2_cutoff = 100
set gyro_stage2_lowpass_hz = 300
set acc_hardware = MPU6500
set acczero_x = 14
set acczero_y = -34
set acczero_z = -23
set accgain_x = 4076
set accgain_y = 4080
set accgain_z = 3961
set rangefinder_hardware = BENEWAKE
set rangefinder_median_filter = ON
set opflow_hardware = MSP
set mag_hardware = FAKE
set baro_hardware = BMP280
set pitot_hardware = NONE
set receiver_type = MSP
set min_throttle = 1030
set motor_pwm_rate = 2000
set motor_pwm_protocol = ONESHOT125
set failsafe_delay = 2
set align_board_yaw = 450
set model_preview_type = 3
set nav_extra_arming_safety = OFF
set name = DroneStormBeta

# profile
profile 1

set mc_i_pitch = 50
set mc_d_pitch = 15
set mc_i_roll = 45
set mc_d_roll = 15
set mc_p_yaw = 50
set dterm_lpf_hz = 100
set dterm_notch_hz = 260
set dterm_notch_cutoff = 160
set rc_yaw_expo = 70
set roll_rate = 75
set pitch_rate = 75
set yaw_rate = 75

# battery_profile
battery_profile 1

set battery_capacity = 460

# 